/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.13-MariaDB : Database - profilexchange
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`profilexchange` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `profilexchange`;

/*Table structure for table `agency` */

DROP TABLE IF EXISTS `agency`;

CREATE TABLE `agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `address` text,
  `phone` varchar(11) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `contact_person` varchar(32) DEFAULT NULL,
  `contact_person_no` varchar(11) DEFAULT NULL,
  `contact_person_email` varchar(32) DEFAULT NULL,
  `interview` varchar(12) DEFAULT NULL,
  `bus_reg_no` varchar(11) DEFAULT NULL,
  `url` text,
  `bank_account_no` varchar(30) DEFAULT NULL,
  `bank_account_name` varbinary(32) DEFAULT NULL,
  `no_listing_allowed` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `agency` */

insert  into `agency`(`id`,`userid`,`name`,`address`,`phone`,`country`,`contact_person`,`contact_person_no`,`contact_person_email`,`interview`,`bus_reg_no`,`url`,`bank_account_no`,`bank_account_name`,`no_listing_allowed`,`status`) values (1,1,'Agency Name','Agency Address','Agency Phon','PH','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(2,2,'Agency Name','Agency Address','Agency Phon','US','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(3,3,'Agency Name','Agency Address','Agency Phon','PH','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(4,4,'Agency Name','Agency Address','Agency Phon','US','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(7,7,'Aeonflux Agency','04c Kampupot St. Puwang Wawa, Taguig City','0912212121','PH','Jan Christian','09468965897','christianaristain@gmail.com','15872585','5458774','http://localhost/addy/index.php/admin/agency/new_agency','55555555','bane name',12,'active');

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=latin1;

/*Data for the table `country` */

insert  into `country`(`id`,`code`,`name`) values (1,'AF','Afghanistan'),(2,'AX','Åland Islands'),(3,'AL','Albania'),(4,'DZ','Algeria'),(5,'AS','American Samoa'),(6,'AD','Andorra'),(7,'AO','Angola'),(8,'AI','Anguilla'),(9,'AQ','Antarctica'),(10,'AG','Antigua and Barbuda'),(11,'AR','Argentina'),(12,'AM','Armenia'),(13,'AW','Aruba'),(14,'AU','Australia'),(15,'AT','Austria'),(16,'AZ','Azerbaijan'),(17,'BS','Bahamas'),(18,'BH','Bahrain'),(19,'BD','Bangladesh'),(20,'BB','Barbados'),(21,'BY','Belarus'),(22,'BE','Belgium'),(23,'BZ','Belize'),(24,'BJ','Benin'),(25,'BM','Bermuda'),(26,'BT','Bhutan'),(27,'BO','Bolivia'),(28,'BA','Bosnia and Herzegovina'),(29,'BW','Botswana'),(30,'BV','Bouvet Island'),(31,'BR','Brazil'),(32,'IO','British Indian Ocean Territory'),(33,'BN','Brunei Darussalam'),(34,'BG','Bulgaria'),(35,'BF','Burkina Faso'),(36,'BI','Burundi'),(37,'KH','Cambodia'),(38,'CM','Cameroon'),(39,'CA','Canada'),(40,'CV','Cape Verde'),(41,'KY','Cayman Islands'),(42,'CF','Central African Republic'),(43,'TD','Chad'),(44,'CL','Chile'),(45,'CN','China'),(46,'CX','Christmas Island'),(47,'CC','Cocos (Keeling) Islands'),(48,'CO','Colombia'),(49,'KM','Comoros'),(50,'CG','Congo'),(51,'CD','Congo, The Democratic Republic'),(52,'CK','Cook Islands'),(53,'CR','Costa Rica'),(54,'CI','Cote D\'ivoire'),(55,'HR','Croatia'),(56,'CU','Cuba'),(57,'CY','Cyprus'),(58,'CZ','Czech Republic'),(59,'DK','Denmark'),(60,'DJ','Djibouti'),(61,'DM','Dominica'),(62,'DO','Dominican Republic'),(63,'EC','Ecuador'),(64,'EG','Egypt'),(65,'SV','El Salvador'),(66,'GQ','Equatorial Guinea'),(67,'ER','Eritrea'),(68,'EE','Estonia'),(69,'ET','Ethiopia'),(70,'FK','Falkland Islands (Malvinas)'),(71,'FO','Faroe Islands'),(72,'FJ','Fiji'),(73,'FI','Finland'),(74,'FR','France'),(75,'GF','French Guiana'),(76,'PF','French Polynesia'),(77,'TF','French Southern Territories'),(78,'GA','Gabon'),(79,'GM','Gambia'),(80,'GE','Georgia'),(81,'DE','Germany'),(82,'GH','Ghana'),(83,'GI','Gibraltar'),(84,'GR','Greece'),(85,'GL','Greenland'),(86,'GD','Grenada'),(87,'GP','Guadeloupe'),(88,'GU','Guam'),(89,'GT','Guatemala'),(90,'GG','Guernsey'),(91,'GN','Guinea'),(92,'GW','Guinea-bissau'),(93,'GY','Guyana'),(94,'HT','Haiti'),(95,'HM','Heard Island and Mcdonald Isla'),(96,'VA','Holy See (Vatican City State)'),(97,'HN','Honduras'),(98,'HK','Hong Kong'),(99,'HU','Hungary'),(100,'IS','Iceland'),(101,'IN','India'),(102,'ID','Indonesia'),(103,'IR','Iran, Islamic Republic of'),(104,'IQ','Iraq'),(105,'IE','Ireland'),(106,'IM','Isle of Man'),(107,'IL','Israel'),(108,'IT','Italy'),(109,'JM','Jamaica'),(110,'JP','Japan'),(111,'JE','Jersey'),(112,'JO','Jordan'),(113,'KZ','Kazakhstan'),(114,'KE','Kenya'),(115,'KI','Kiribati'),(116,'KP','Korea, Democratic People\'s Rep'),(117,'KR','Korea, Republic of'),(118,'KW','Kuwait'),(119,'KG','Kyrgyzstan'),(120,'LA','Lao People\'s Democratic Republ'),(121,'LV','Latvia'),(122,'LB','Lebanon'),(123,'LS','Lesotho'),(124,'LR','Liberia'),(125,'LY','Libyan Arab Jamahiriya'),(126,'LI','Liechtenstein'),(127,'LT','Lithuania'),(128,'LU','Luxembourg'),(129,'MO','Macao'),(130,'MK','Macedonia, The Former Yugoslav'),(131,'MG','Madagascar'),(132,'MW','Malawi'),(133,'MY','Malaysia'),(134,'MV','Maldives'),(135,'ML','Mali'),(136,'MT','Malta'),(137,'MH','Marshall Islands'),(138,'MQ','Martinique'),(139,'MR','Mauritania'),(140,'MU','Mauritius'),(141,'YT','Mayotte'),(142,'MX','Mexico'),(143,'FM','Micronesia, Federated States o'),(144,'MD','Moldova, Republic of'),(145,'MC','Monaco'),(146,'MN','Mongolia'),(147,'ME','Montenegro'),(148,'MS','Montserrat'),(149,'MA','Morocco'),(150,'MZ','Mozambique'),(151,'MM','Myanmar'),(152,'NA','Namibia'),(153,'NR','Nauru'),(154,'NP','Nepal'),(155,'NL','Netherlands'),(156,'AN','Netherlands Antilles'),(157,'NC','New Caledonia'),(158,'NZ','New Zealand'),(159,'NI','Nicaragua'),(160,'NE','Niger'),(161,'NG','Nigeria'),(162,'NU','Niue'),(163,'NF','Norfolk Island'),(164,'MP','Northern Mariana Islands'),(165,'NO','Norway'),(166,'OM','Oman'),(167,'PK','Pakistan'),(168,'PW','Palau'),(169,'PS','Palestinian Territory, Occupie'),(170,'PA','Panama'),(171,'PG','Papua New Guinea'),(172,'PY','Paraguay'),(173,'PE','Peru'),(174,'PH','Philippines'),(175,'PN','Pitcairn'),(176,'PL','Poland'),(177,'PT','Portugal'),(178,'PR','Puerto Rico'),(179,'QA','Qatar'),(180,'RE','Reunion'),(181,'RO','Romania'),(182,'RU','Russian Federation'),(183,'RW','Rwanda'),(184,'SH','Saint Helena'),(185,'KN','Saint Kitts and Nevis'),(186,'LC','Saint Lucia'),(187,'PM','Saint Pierre and Miquelon'),(188,'VC','Saint Vincent and The Grenadin'),(189,'WS','Samoa'),(190,'SM','San Marino'),(191,'ST','Sao Tome and Principe'),(192,'SA','Saudi Arabia'),(193,'SN','Senegal'),(194,'RS','Serbia'),(195,'SC','Seychelles'),(196,'SL','Sierra Leone'),(197,'SG','Singapore'),(198,'SK','Slovakia'),(199,'SI','Slovenia'),(200,'SB','Solomon Islands'),(201,'SO','Somalia'),(202,'ZA','South Africa'),(203,'GS','South Georgia and The South Sa'),(204,'ES','Spain'),(205,'LK','Sri Lanka'),(206,'SD','Sudan'),(207,'SR','Suriname'),(208,'SJ','Svalbard and Jan Mayen'),(209,'SZ','Swaziland'),(210,'SE','Sweden'),(211,'CH','Switzerland'),(212,'SY','Syrian Arab Republic'),(213,'TW','Taiwan, Province of China'),(214,'TJ','Tajikistan'),(215,'TZ','Tanzania, United Republic of'),(216,'TH','Thailand'),(217,'TL','Timor-leste'),(218,'TG','Togo'),(219,'TK','Tokelau'),(220,'TO','Tonga'),(221,'TT','Trinidad and Tobago'),(222,'TN','Tunisia'),(223,'TR','Turkey'),(224,'TM','Turkmenistan'),(225,'TC','Turks and Caicos Islands'),(226,'TV','Tuvalu'),(227,'UG','Uganda'),(228,'UA','Ukraine'),(229,'AE','United Arab Emirates'),(230,'GB','United Kingdom'),(231,'US','United States'),(232,'UM','United States Minor Outlying I'),(233,'UY','Uruguay'),(234,'UZ','Uzbekistan'),(235,'VU','Vanuatu'),(236,'VE','Venezuela'),(237,'VN','Viet Nam'),(238,'VG','Virgin Islands, British'),(239,'VI','Virgin Islands, U.S.'),(240,'WF','Wallis and Futuna'),(241,'EH','Western Sahara'),(242,'YE','Yemen'),(243,'ZM','Zambia'),(244,'ZW','Zimbabwe');

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `news` */

insert  into `news`(`id`,`news`) values (1,'(This will be edited manually by Administrator, all agency and recruiter will see this on their homepage),\r\n\r\nEtiam nisi lorem, posuere at turpis at, fringilla efficitur quam. Suspendisse vitae lacus ac lectus facilisis ornare. Vivamus vitae pulvinar nisi, in vehicula elit. Praesent iaculis ante tellus, eu mattis lectus suscipit sit amet. Sed congue accumsan nunc in iaculis. \r\n\r\nSed malesuada elit turpis, eu egestas eros rhoncus non. Sed pulvinar euismod libero sit amet scelerisque. Vestibulum ante felis, condimentum in vulputate id, tempor eu nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. \r\n\r\nEtiam condimentum augue vitae nulla suscipit eleifend. Phasellus consectetur volutpat nulla, cursus facilisis elit vulputate sed. Mauris in semper sem. Interdum et malesuada fames ac ante ipsum primis in faucibus.\r\n');

/*Table structure for table `profile_area_of_work` */

DROP TABLE IF EXISTS `profile_area_of_work`;

CREATE TABLE `profile_area_of_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `care_of_infants_0to2` tinyint(4) DEFAULT '0',
  `care_of_infants_3to5` tinyint(4) DEFAULT '0',
  `care_of_infants_6to8` tinyint(4) DEFAULT '0',
  `care_of_infants_9to11` tinyint(4) DEFAULT '0',
  `care_of_infants_12to14` tinyint(4) DEFAULT '0',
  `care_for_elderly` tinyint(4) DEFAULT '0',
  `care_for_disabled` tinyint(4) DEFAULT '0',
  `general_housework` tinyint(4) DEFAULT '0',
  `cooking` tinyint(4) DEFAULT '0',
  `language_spoken` text,
  `other_skills` text,
  `num_care_of_infants_0to2` int(11) DEFAULT NULL,
  `num_care_of_infants_3to5` int(11) DEFAULT NULL,
  `num_care_of_infants_6to8` int(11) DEFAULT NULL,
  `num_care_of_infants_9to11` int(11) DEFAULT NULL,
  `num_care_of_infants_12to14` int(11) DEFAULT NULL,
  `num_care_for_elderly` int(11) DEFAULT NULL,
  `num_care_for_disabled` int(11) DEFAULT NULL,
  `num_general_housework` int(11) DEFAULT NULL,
  `num_cooking` int(11) DEFAULT NULL,
  `rate_care_of_infants_0to2` int(11) DEFAULT NULL,
  `rate_care_of_infants_3to5` int(11) DEFAULT NULL,
  `rate_care_of_infants_6to8` int(11) DEFAULT NULL,
  `rate_care_of_infants_9to11` int(11) DEFAULT NULL,
  `rate_care_of_infants_12to14` int(11) DEFAULT NULL,
  `rate_care_for_elderly` int(11) DEFAULT NULL,
  `rate_care_for_disabled` int(11) DEFAULT NULL,
  `rate_general_housework` int(11) DEFAULT NULL,
  `rate_cooking` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `profile_area_of_work` */

insert  into `profile_area_of_work`(`id`,`profile_id`,`care_of_infants_0to2`,`care_of_infants_3to5`,`care_of_infants_6to8`,`care_of_infants_9to11`,`care_of_infants_12to14`,`care_for_elderly`,`care_for_disabled`,`general_housework`,`cooking`,`language_spoken`,`other_skills`,`num_care_of_infants_0to2`,`num_care_of_infants_3to5`,`num_care_of_infants_6to8`,`num_care_of_infants_9to11`,`num_care_of_infants_12to14`,`num_care_for_elderly`,`num_care_for_disabled`,`num_general_housework`,`num_cooking`,`rate_care_of_infants_0to2`,`rate_care_of_infants_3to5`,`rate_care_of_infants_6to8`,`rate_care_of_infants_9to11`,`rate_care_of_infants_12to14`,`rate_care_for_elderly`,`rate_care_for_disabled`,`rate_general_housework`,`rate_cooking`) values (1,1,1,1,1,1,1,1,1,1,1,'English','',0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1),(2,2,1,1,1,1,1,1,1,1,1,'English','',0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1),(3,3,1,1,1,1,1,1,1,1,1,'English','',0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1);

/*Table structure for table `profile_employment_history` */

DROP TABLE IF EXISTS `profile_employment_history`;

CREATE TABLE `profile_employment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `employer` text,
  `date_from` text,
  `date_to` text,
  `country` text,
  `work_duties` text,
  `remarks` text,
  `emp_feedback` text,
  `prev_work_sg` tinyint(4) DEFAULT NULL,
  `availability_fdw` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `profile_employment_history` */

insert  into `profile_employment_history`(`id`,`profile_id`,`employer`,`date_from`,`date_to`,`country`,`work_duties`,`remarks`,`emp_feedback`,`prev_work_sg`,`availability_fdw`) values (1,1,'dadas','2016-06-01','2016-06-01',NULL,'gdf','gdf','fd',1,'[\"FDW is not available for interview\"]'),(2,2,'dadas','2016-06-01','2016-06-01',NULL,'gdf','gdf','fd',1,'[\"FDW is not available for interview\"]'),(3,3,'dsa','2016-06-01','2016-06-08',NULL,'das','dsa','das',1,'[\"FDW can be interviewed in person\"]');

/*Table structure for table `profile_medical_history` */

DROP TABLE IF EXISTS `profile_medical_history`;

CREATE TABLE `profile_medical_history` (
  `int` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `allergies` text,
  `illness` text COMMENT 'Past and existing illnesses (including chronic ailments and illnesses requiring medication):\r\nCHECKBOX (Mental illness, Tuberculosis, Epilepsy, Heart disease, Asthma, Malaria, Diabetes, Operations, Hypertension, Others)',
  `disabilities` text COMMENT 'Physical disablilities',
  `dietary_restrictions` text,
  `food_preferences` text COMMENT 'DROPDOWN (No pork, No beef, Vegetarian, Others)',
  `rest_day` int(11) DEFAULT NULL COMMENT 'Preference for rest day per month',
  `others` text,
  `evaluation_skill` text COMMENT 'Please indicate the method(s) used to evaluate the FDW''s skills (can tick more than one):',
  PRIMARY KEY (`int`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `profile_medical_history` */

insert  into `profile_medical_history`(`int`,`profile_id`,`allergies`,`illness`,`disabilities`,`dietary_restrictions`,`food_preferences`,`rest_day`,`others`,`evaluation_skill`) values (1,1,'Hatching','Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma','Physical disablilities:','Dietary restrictions:','No beef',3,'Any other remarks:','Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by Singapore EA|Interviewed by overseas training centre/EA'),(2,2,'Hatching','Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma','Physical disablilities:','Dietary restrictions:','No beef',3,'Any other remarks:','Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by Singapore EA|Interviewed by overseas training centre/EA'),(3,3,'','','','','No pork',0,'','');

/*Table structure for table `profile_personal_info` */

DROP TABLE IF EXISTS `profile_personal_info`;

CREATE TABLE `profile_personal_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `profile_owner` int(11) DEFAULT '0',
  `owner_id` int(11) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(32) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationality` varchar(32) DEFAULT NULL,
  `home_address` text,
  `airport` varchar(32) DEFAULT NULL,
  `home_number` varchar(32) DEFAULT NULL,
  `religion` varchar(32) DEFAULT NULL COMMENT 'CHRISTIANITY, ISLAM, HINDUISM, TAOISM, BUDDHISM, SHINTO, SIKHISM, JUDAISM, AGNOSTIC, ATHEIST',
  `education` varchar(32) DEFAULT NULL,
  `no_of_siblings` int(11) DEFAULT NULL,
  `marital_status` varchar(32) DEFAULT NULL COMMENT 'SINGLE, IN A RELATIONSHIP, ENGAGED, MARRIED, DIVORCED, WIDOWED',
  `no_of_children` int(11) DEFAULT NULL,
  `age_of_childing` varchar(32) DEFAULT NULL,
  `country` varchar(12) DEFAULT NULL,
  `time_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `profile_personal_info` */

insert  into `profile_personal_info`(`id`,`name`,`profile_owner`,`owner_id`,`date_of_birth`,`place_of_birth`,`height`,`weight`,`age`,`nationality`,`home_address`,`airport`,`home_number`,`religion`,`education`,`no_of_siblings`,`marital_status`,`no_of_children`,`age_of_childing`,`country`,`time_added`) values (1,'Jan Christian',7,NULL,'1993-01-21','Taguig',166,15,23,'Filipino','Residential address in home country:','NAIA','09468965897','CHRISTIANITY','DEGREE',12,'SINGLE',1,'1','PH','2016-06-26 15:45:33'),(2,'Jan Christians',7,NULL,'1993-01-21','Taguig',166,22222,112,'Filipino','Residential address in home country:','NAIA','09468965897','CHRISTIANITY','DEGREE',12,'SINGLE',1,'1','US','2016-06-26 15:45:33'),(3,'',0,NULL,'0000-00-00','',0,0,0,'','','','','','',0,'',0,'','AF','2016-06-26 16:05:08');

/*Table structure for table `recruiter` */

DROP TABLE IF EXISTS `recruiter`;

CREATE TABLE `recruiter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(15) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `address` text,
  `phone` varchar(11) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `contact_person` varchar(32) DEFAULT NULL,
  `contact_person_no` varchar(11) DEFAULT NULL,
  `contact_person_email` varchar(32) DEFAULT NULL,
  `interview` varchar(12) DEFAULT NULL,
  `bus_reg_no` varchar(11) DEFAULT NULL,
  `url` text,
  `bank_account_no` varchar(30) DEFAULT NULL,
  `bank_account_name` varbinary(32) DEFAULT NULL,
  `no_booking_allowed` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `recruiter` */

insert  into `recruiter`(`id`,`userid`,`name`,`address`,`phone`,`country`,`contact_person`,`contact_person_no`,`contact_person_email`,`interview`,`bus_reg_no`,`url`,`bank_account_no`,`bank_account_name`,`no_booking_allowed`,`status`) values (1,1,'Recruiter Name','Recruiter Address','Recruiter P','PH','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(2,3,'12','Recruiter Addressdsa','Recruiter P','GB','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(3,2,'Recruiter Name','Recruiter Address','Recruiter P','PH','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(5,4,'Recruiter Name','Recruiter Address','Recruiter P','US','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(6,5,'Recruiter Name','Recruiter Address','Recruiter P','PH','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`user_type`,`status`) values (1,'Username','dc647eb65e6711e155375218212b3964',2,1),(2,'Usernamed','dc647eb65e6711e155375218212b3964',2,1),(3,'Usernames','dc647eb65e6711e155375218212b3964',2,1),(4,'Usernamedasdas','dc647eb65e6711e155375218212b3964',2,1),(5,'Usernamedasd','dc647eb65e6711e155375218212b3964',2,1),(6,'admin','5f4dcc3b5aa765d61d8327deb882cf99',0,1),(7,'aeonflux','7cf253e55693c11aacd6203042d8b19d',0,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

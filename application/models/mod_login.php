<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_login extends CI_Model {

	public function login($data = array()){
		
		$data = array(
						'username' 	=> $data['username'], 
						'password' 	=> md5($data['password']),
						'status'  	=> 1);
						
		$this->db->where($data);
		
		$result = $this->db->get("user");
		
		return $result->result();
	}
	
	public function validate($intUserID = 0, $intUserType = 0){
		$table = array("admin", "recruiter", "agency");
		
		$sql = "SELECT `status` 
					FROM ".$table[$intUserType]."
					WHERE `status` IN('inactive', 'suspended')
					AND userid = ".$intUserID." ";
					
		$result = $this->db->query($sql);

		return $result->result();
	}
	
	public function create_session($data = array()){
		$arrSession = array(
							"isLoggedin"	=> TRUE,
							"userid" 		=> $data['userid'],
							"username" 		=> $data['username'],
							"role"			=> $data["role"]
							);
		
		if(isset($data['logo'])){
			$arrSession['logo'] = $data['logo'];
		}
		$this->session->set_userdata($arrSession);
		
		return false;
	}
	
}

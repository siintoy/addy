<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_profile extends CI_Model {

	public function add($data = array(), $table = ""){
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	
	public function update($data = array(), $where = array(), $table = ""){
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	
	
	public function get_agencies($data = array()){
		$sql = "SELECT 
				  id,
				  `name` 
				FROM
				  agency 
				ORDER BY NAME ";
				
		return $this->db->query($sql)->result();
	}
	
	public function getByCountry($data = array()){
		$sql = "SELECT 
				  b.`name`,
				  b.`code`,
				  COUNT(*) `count` 
				FROM
				  `profile_personal_info` a 
				  JOIN `country` b 
				ON a.`country` = b.`code` 
				WHERE a.profile_owner = {$data['id']}  AND isPublish = 1
				GROUP BY b.`code` 
				ORDER BY b.name ASC  ";
				
		return $this->db->query($sql)->result();
	}
	
	public function getProfileByCountry($code = "", $id = 0){
		$sql = "SELECT 
				  *
				FROM
				  `profile_personal_info` a 
				  LEFT JOIN profile_media b 
				   ON b.profile_id = a.id
				WHERE a.`country` = '{$code}' AND isPublish = 1
				AND profile_owner = {$id}
				LIMIT 6";
				
		return $this->db->query($sql)->result();
	}
	
	public function getLatestProfile($id = 0){
		$sql = "SELECT 
				  * 
				FROM
				  `profile_personal_info` a 
				  LEFT JOIN profile_media b 
				   ON b.profile_id = a.id
				  WHERE profile_owner = {$id} AND isPublish = 1
				ORDER BY a.`time_added` DESC 
				LIMIT 6";
				
		return $this->db->query($sql)->result();
	}
	
	public function get_settings($id = 0){
		$this->db->where("agency_id", $id);
		return $this->db->get("agency_settings")->result();
	}
	
	public function get_faqs($id = 0){
		$this->db->where("agency_id", $id);
		return $this->db->get("faqs")->result();
	}
	
	public function getAgencyName($id = 0){
		$this->db->where("id", $id);
		return $this->db->get("agency")->result();
	}
	
	public function get_search_profiles($search = array(), $age = array(), $id = 0){
		
		$this->db->like($search);
		$this->db->where('profile_owner', $id);
		$this->db->where('isPublish', 1);
		if(isset($age[1])){
			$this->db->where("age BETWEEN ".$age[0]." AND ".$age[1]."");
		}
		$this->db->select('
						  `a`.`id` AS emp_id,
						  `a`.`id` AS profile_id,
						  `name` AS profname,
						  `profile_owner`,
						  `owner_id`,
						  `date_of_birth`,
						  `place_of_birth`,
						  `height`,
						  `weight`,
						  `age`,
						  `nationality`,
						  `home_address`,
						  `airport`,
						  `home_number`,
						  `religion`,
						  `education`,
						  `no_of_siblings`,
						  `marital_status`,
						  `no_of_children`,
						  `age_of_childing`,
						  `country`,
						  `time_added`,
						  `isPublish`,
						  b.*');
		$this->db->join('profile_media b', 'b.profile_id = a.id');
		return $this->db->get("profile_personal_info a")->result();
	}
	
	public function getProfileByAgencyID($id = 0){
		if($this->session->userdata("role") > 0){
			$this->db->where("profile_owner", $id);
		}
		$this->db->order_by("name asc");
		return $this->db->get("profile_personal_info");
	}
	
	public function view_profile($agency_id = 0, $profile_id = 0){
		$where = ($this->session->userdata("role") > 0 ) 
						? "WHERE info.`profile_owner` = {$agency_id} 
							AND info.id = {$profile_id};"
						: "WHERE  info.id = {$profile_id};";
		
		$qry = "SELECT 
				 info.`name` AS profname,
				  `profile_owner`,
				  `owner_id`,
				  `date_of_birth`,
				  `place_of_birth`,
				  `height`,
				  `weight`,
				  `age`,
				  `nationality`,
				  `home_address`,
				  `airport`,
				  `home_number`,
				  `religion`,
				  `education`,
				  `no_of_siblings`,
				  `marital_status`,
				  `no_of_children`,
				  `age_of_childing`,
				  `country`,
				  `time_added`,
				  `isPublish`,
				  `care_of_infants_0to2`,
				  `care_of_infants_3to5`,
				  `care_of_infants_6to8`,
				  `care_of_infants_9to11`,
				  `care_of_infants_12to14`,
				  `care_for_elderly`,
				  `care_for_disabled`,
				  `general_housework`,
				  `cooking`,
				  `language_spoken`,
				  `other_skills`,
				  `num_care_of_infants_0to2`,
				  `num_care_of_infants_3to5`,
				  `num_care_of_infants_6to8`,
				  `num_care_of_infants_9to11`,
				  `num_care_of_infants_12to14`,
				  `num_care_for_elderly`,
				  `num_care_for_disabled`,
				  `num_general_housework`,
				  `num_cooking`,
				  `rate_care_of_infants_0to2`,
				  `rate_care_of_infants_3to5`,
				  `rate_care_of_infants_6to8`,
				  `rate_care_of_infants_9to11`,
				  `rate_care_of_infants_12to14`,
				  `rate_care_for_elderly`,
				  `rate_care_for_disabled`,
				  `rate_general_housework`,
				  `rate_cooking`,
				  `language`, 
				  `date_to`,
				  `country_history`,
				  `work_duties`,
				  `remarks`,
				  `emp_feedback`,
				  `prev_work_sg`,
				  `availability_fdw`,
				  `allergies`,
				  `illness`,
				  `disabilities`,
				  `dietary_restrictions`,
				  `food_preferences`,
				  `rest_day`,
				  `others`,
				  `evaluation_skill`,
				  emp_history.id emp_id,
				  country.name as country_name,
				  fee,
				 emp_history.id emp_id,
				 media.*
				FROM
				  `profile_personal_info` info 
				  JOIN `profile_area_of_work` `work` 
					ON work.`profile_id` = info.`id` 
				 LEFT JOIN `profile_employment_history` emp_history 
					ON emp_history.`profile_id` = info.`id` 
				  JOIN `profile_medical_history` medical 
					ON medical.`profile_id` = info.`id`
				LEFT JOIN `profile_media` media 
					ON media.`profile_id` = info.`id` 
				LEFT JOIN `country` country 
					ON country.`code` = info.`country` 		
					{$where} ";
				  
		return $this->db->query($qry)->result();
	}
	
	public function search_results($strWhere = "",$id = 0){
		$qry = "SELECT 
				  info.`name` AS profname,
				  `profile_owner`,
				  `owner_id`,
				  `date_of_birth`,
				  `place_of_birth`,
				  `height`,
				  `weight`,
				  `age`,
				  `nationality`,
				  `home_address`,
				  `airport`,
				  `home_number`,
				  `religion`,
				  `education`,
				  `no_of_siblings`,
				  `marital_status`,
				  `no_of_children`,
				  `age_of_childing`,
				  `country`,
				  `time_added`,
				  `isPublish`,
				  `care_of_infants_0to2`,
				  `care_of_infants_3to5`,
				  `care_of_infants_6to8`,
				  `care_of_infants_9to11`,
				  `care_of_infants_12to14`,
				  `care_for_elderly`,
				  `care_for_disabled`,
				  `general_housework`,
				  `cooking`,
				  `language_spoken`,
				  `other_skills`,
				  `num_care_of_infants_0to2`,
				  `num_care_of_infants_3to5`,
				  `num_care_of_infants_6to8`,
				  `num_care_of_infants_9to11`,
				  `num_care_of_infants_12to14`,
				  `num_care_for_elderly`,
				  `num_care_for_disabled`,
				  `num_general_housework`,
				  `num_cooking`,
				  `rate_care_of_infants_0to2`,
				  `rate_care_of_infants_3to5`,
				  `rate_care_of_infants_6to8`,
				  `rate_care_of_infants_9to11`,
				  `rate_care_of_infants_12to14`,
				  `rate_care_for_elderly`,
				  `rate_care_for_disabled`,
				  `rate_general_housework`,
				  `rate_cooking`,
				  `language`, 
				  `date_to`,
				  `country_history`,
				  `work_duties`,
				  `remarks`,
				  `emp_feedback`,
				  `prev_work_sg`,
				  `availability_fdw`,
				  `allergies`,
				  `illness`,
				  `disabilities`,
				  `dietary_restrictions`,
				  `food_preferences`,
				  `rest_day`,
				  `others`,
				  `evaluation_skill`,
				  emp_history.id emp_id,
				  fee,
				  country.name as country_name,
				  media.*
				FROM
				  `profile_personal_info` info 
				  JOIN `profile_area_of_work` `work` 
					ON work.`profile_id` = info.`id` 
				LEFT JOIN `profile_employment_history` emp_history 
					ON emp_history.`profile_id` = info.`id` 
				  JOIN `profile_medical_history` medical 
					ON medical.`profile_id` = info.`id`
				LEFT JOIN `profile_media` media 
					ON media.`profile_id` = info.`id`
				LEFT JOIN `country` country 
					ON country.`code` = info.`country` 					
				WHERE 1 {$strWhere} AND profile_owner = {$id}";
				  
		return $this->db->query($qry)->result();
	}
	
	public function getNationality($id = 0){
		$qry = "SELECT DISTINCT 
				  (nationality) 
				FROM
				  `profile_personal_info` a 
				WHERE a.`profile_owner` = {$id} 
				  AND a.`isPublish` = 1
				ORDER BY 1 ";
				  
		return $this->db->query($qry)->result();
	}
	
	public function getCountries($id = 0){
		$qry = "SELECT DISTINCT 
				   (b.`name`),
					b.`code`
				FROM
				  `profile_personal_info` a 
				  JOIN `country` b
				   ON b.`code` = a.`country`
				WHERE a.`profile_owner` = {$id} 
				 AND a.`isPublish` = 1
				ORDER BY 1 ";
				  
		return $this->db->query($qry)->result();
	}
}

<style>
.lastest_profile{
	display:inline-table;
}
</style>
<div class="container-fluid">
<div class="navbar navbar-default navbar-inverse navbar-fixed-top">
      <div class="container">
          <div>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img style="margin-top:-4px;" width="85px;" height="30px" src="<?php echo base_url();?>uploads/<?php echo $settings[0]->logo; ?>"/></a>
        </div>
        <div class="navbar-collapse collapse">
          	<ul class="nav navbar-nav Xnavbar-right">
		<!-- AGENCY -->
			<?php if(isset($settings)): ?>
				<li>
					<a href="#"><?php echo $settings[0]->serial_no; ?></a>
				</li>
			<?php endif; ?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="<?php echo site_url("main?id={$id}")?>">HOME</a>
				</li>
				<li>
					<a data-toggle="modal" data-target="#myModalAbout" href="#">ABOUT US</a>
				</li>
				<li>
					<a href="<?php echo site_url("main/advance_search?id={$id}");?>">ADVANCE SEARCH</a>
				</li>
				<li>
					<a data-toggle="modal" data-target="#myModalnews" href="#">LATEST NEWS</a>
				</li>
				<li>
					<a data-toggle="modal" data-target="#myModalContactUs" href="#">CONTACT US</a>
				</li>
				<li>
					<a href="<?php echo site_url("main/faqs?id={$id}"); ?>">FAQs</a>
				</li>
		<!-- AGENCY -->
				<li>
					<a href="<?php echo site_url($this->session->userdata('isLoggedin') ? "logout" : "login" );?>"><?php echo ($this->session->userdata('isLoggedin') ? "LOGOUT" : "LOGIN" );?></a>
				</li>

			</ul>
		</div>
        </div><!--/.nav-collapse -->
        </div>
        </div>
    </div>
	<div style="background:white;"  class="row">
		
		<div style="margin-top:50px" class="page-header">
		<?php if(isset($header)): ?>
			<div><img class="img-thumbnail" style="height:150px;" width="100%" src="<?php echo base_url(); ?>uploads/<?php echo $settings[0]->banner; ?>"/></div>
		<?php endif;?> 
		</div>
		<div class="col-md-12">
			<div class="row">
				<?php echo isset($body_content) ? $body_content : false; ?>
			</div>
		</div>
	</div>
</div>


  <div class="modal fade" id="myModalContactUs" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">CONTACT US</h4>
        </div>
        <div class="modal-body">
          <div class="">
			 <form role="form" method="post" action="<?php echo base_url();?>index.php/main/contact_us?id=<?php echo $id; ?>">
				  <div class="form-group">
					<label for="name">Your Name:</label>
					<input required type="text"  placeholder="Your Name"  class="form-control" id="name" name="name">
				  </div>
				  
				  <div class="form-group">
					<label for="email">Your Email:</label>
					<input required type="email"  placeholder="Your Email"  class="form-control" id="email" name="email">
				  </div>
				  
				  <div class="form-group">
					<label for="phone">Your Phone:</label>
					<input required type="text"  placeholder="Your Phone"  class="form-control" id="phone" name="phone">
				  </div>
				  
				  <div class="form-group">
					<label for="message">Message:</label>
					<textarea required class="form-control" id="message" placeholder="Message" name="message"></textarea>
				  </div>
				  
				  <div class="form-group">
				  </div>

			</div>
        </div>
        <div class="modal-footer">
			<input type="submit" class="btn btn-primary"  value="Submit" />
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
	 </form>
      </div>
      
    </div>
  </div>
  
  <div class="modal fade" id="myModalAbout" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ABOUT US</h4>
        </div>
        <div class="modal-body">
          <?php echo html_entity_decode(nl2br($settings[0]->about_us), ENT_QUOTES); ?>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <div class="modal fade" id="myModalFAQs" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ABOUT US</h4>
        </div>
        <div class="modal-body">
          <?php echo html_entity_decode(nl2br($settings[0]->about_us), ENT_QUOTES); ?>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <div class="modal fade" id="myModalnews" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">News & Announcement</h4>
        </div>
        <div class="modal-body">
          <?php echo html_entity_decode(nl2br($settings[0]->news), ENT_QUOTES); ?>
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <br>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_user extends CI_Model {

	public function add($data = array()){
		$this->db->insert("user",$data);
		return $this->db->insert_id();
	}
	
	public function hasData($username = ""){
		$this->db->where("username",$username);
		$result = $this->db->get("user");
		return (count($result->result()) > 0 ? true : false);
	}
	
}

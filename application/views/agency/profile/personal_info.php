<?php if($flash = $this->session->flashdata('flsh_msg')): ?>
  <div class="<?php echo $flash['class']; ?>">
    <a href="#" class="close" data-dismiss="<?php echo $flash['dismiss']; ?>" aria-label="close">&times;</a>
    <strong><?php echo $flash['errType']; ?>!: </strong> <?php echo $flash['msg']; ?>.
  </div> 
<?php endif; ?>
<div class="form-group">
	<label for="fee">Chargeable Fee:</label>
    <span style="color:red; min-width:100px;">
    <b><span id="myValue"></span></b>
    </span>
    <input type="range" id="myRange" name="personal_info[fee]" value="<?php echo $info[0]->fee == 0 ? "0" : $info[0]->fee; ?>" max="10000" min="0" style="width:100%"> 
</div>

<div class="form-group">
<?php if($this->session->userdata("role") == 0 OR $this->session->userdata("role") == 1): ?>
	<label for="profile_owner"><?php echo ($this->session->userdata("role") == 0 ? "Profile Owner:" : "Transfer Profile: "); ?></label>
	<select class="form-control"  name="personal_info[profile_owner]" id="profile_owner">
			<option  value="0">No Agency</option>
		<?php foreach($agencies as $rows): ?>
			<option <?php echo ($rows->id == $info[0]->profile_owner)  ? " selected " : false ; ?> value="<?php echo $rows->id; ?>"><?php echo $rows->name; ?></option>
		<?php endforeach; ?>
	</select>
<?php endif; ?>
</div>

<div class="form-group">
	<label for="name">Name:</label>
	<input required type="text"  value="<?php echo isset($info[0]->profname) ? $info[0]->profname : false ;?>"  placeholder="Name"  class="form-control" id="name" name="personal_info[name]" />
</div>
<div class="form-group">
	<label for="datepicker">Date of Birth:</label>
	<input required type="text" value="<?php echo isset($info[0]->date_of_birth) ? $info[0]->date_of_birth : false ;?>" placeholder="Date of Birth"  class="form-control datepicker" id="datepicker" name="personal_info[date_of_birth]" />
</div>

<div class="form-group">
	<label for="place_of_birth">Place of Birth:</label>
	<input required type="text" value="<?php echo isset($info[0]->place_of_birth) ? $info[0]->place_of_birth : false ;?>" placeholder="Place of Birth"  class="form-control" id="place_of_birth" name="personal_info[place_of_birth]" />
</div>

<div class="form-group">
	<label for="country">Country:</label>
	<select required name="personal_info[country]" id="country" class="form-control">
		<?php foreach($country as $countries): ?>
		<option <?php echo ($info[0]->country == $countries->code) ? " selected " : false ;?> value="<?php echo $countries->code; ?>"><?php echo $countries->name; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="age">Age:</label>
	<select  class="form-control" required name="personal_info[age]" id="age">
			<option  value="">Please Select Age</option>
		<?php for($x = 17;$x <= 120;$x++): ?>
			<option <?php echo ($info[0]->age == $x) ? " selected " : false ;?> value="<?php echo $x; ?>"><?php echo $x; ?></option>
		<?php endfor; ?>
	</select>
</div>

<div class="form-group">
	<label for="height">Height:</label>
	<input value="<?php echo isset($info[0]->height) ? $info[0]->height : false ;?>" required type="text" onkeypress='return isNumberKey(event);' placeholder="Height"  class="form-control" id="height" name="personal_info[height]" />
</div>

<div class="form-group">
	<label for="weight">Weight:</label>
	<input value="<?php echo isset($info[0]->weight) ? $info[0]->weight : false ;?>" required type="text" onkeypress='return isNumberKey(event);' placeholder="Weight"  class="form-control" id="weight" name="personal_info[weight]" />
</div>

<div class="form-group">
	<label for="nationality">Nationality:</label>
	<input value="<?php echo isset($info[0]->nationality) ? $info[0]->nationality : false ;?>" required type="text"  placeholder="Nationality"  class="form-control" id="nationality" name="personal_info[nationality]" />
</div>

<div class="form-group">
	<label for="home_address">Residential address in home country:</label>
	<textarea required type="text"  placeholder="Residential address in home country"  class="form-control" id="home_address" name="personal_info[home_address]" ><?php echo isset($info[0]->home_address) ? $info[0]->home_address : false ;?></textarea>
</div>

<div class="form-group">
	<label for="airport">Name of port / airport to be repatriated to:</label>
	<input value="<?php echo isset($info[0]->airport) ? $info[0]->airport : false ;?>" required type="text"  placeholder="Name of port / airport to be repatriated to"  class="form-control" id="airport" name="personal_info[airport]" />
</div>

<div class="form-group">
	<label for="home_number">Contact number in home country:</label>
	<input value="<?php echo isset($info[0]->home_number) ? $info[0]->home_number : false ;?>" required type="text"  placeholder="Contact number in home country"  class="form-control" id="home_number" name="personal_info[home_number]" />
</div>

<div class="form-group">
	<label for="religion">Religion:</label>
	<select  class="form-control" required name="personal_info[religion]" id="religion">
			<option  value="">Please Select Religion</option>
		<?php foreach($rel as $e): ?>
			<option <?php echo ($info[0]->religion == $e) ? " selected " : false ;?> value="<?php echo $e; ?>"><?php echo $e; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="education">Education level:</label>
	<select  class="form-control" required name="personal_info[education]" id="education">
			<option  value="">Please Select Education</option>
		<?php foreach($educ as $w): ?>
			<option <?php echo ($info[0]->education == $w) ? " selected " : false ;?> value="<?php echo $w; ?>"><?php echo $w; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="no_of_siblings">Number of siblings:</label>
	<input value="<?php echo isset($info[0]->no_of_siblings) ? $info[0]->no_of_siblings : false ;?>" required type="text" onkeypress='return isNumberKey(event);' placeholder="Number of siblings"  class="form-control" id="no_of_siblings" name="personal_info[no_of_siblings]" />
</div>

<div class="form-group">
	<label for="marital_status">Marital status:</label>
	<select  class="form-control" required name="personal_info[marital_status]" id="marital_status">
			<option  value="">Please Select Education</option>
		<?php foreach($stat as $q): ?>
			<option <?php echo ($info[0]->marital_status == $q) ? " selected " : false ;?> value="<?php echo $q; ?>"><?php echo $q; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="no_of_children">Number of Children:</label>
	<input value="<?php echo isset($info[0]->no_of_children) ? $info[0]->no_of_children : false ;?>" required type="text"  placeholder="Number of Children" onkeypress='return isNumberKey(event);' class="form-control" id="no_of_children" name="personal_info[no_of_children]" />
</div>

<div class="form-group">
	<label for="age_of_childing">Age(s) of children (if any):</label>
	<input value="<?php echo isset($info[0]->age_of_childing) ? $info[0]->age_of_childing : false ;?>" type="text"   placeholder="Age(s) of children (if any)"  class="form-control" id="age_of_childing" name="personal_info[age_of_childing]" />
</div>

  <script type="text/javascript" charset="utf-8">
var myRange = document.querySelector('#myRange');
var myValue = document.querySelector('#myValue');
var myUnits = 'SGD';
var off = myRange.offsetWidth / (parseInt(myRange.max) - parseInt(myRange.min));
var px =  ((myRange.valueAsNumber - parseInt(myRange.min)) * off) - (myValue.offsetParent.offsetWidth / 2);
  myValue.parentElement.style.left = px + 'px';
  myValue.parentElement.style.top = myRange.offsetHeight + 'px';
  var p = "<?php echo number_format($info[0]->fee, 2); ?>";
  myValue.innerHTML =  '$ '+ p + ' ' + myUnits;

  myRange.oninput =function(){
    let px = ((myRange.valueAsNumber - parseInt(myRange.min)) * off) - (myValue.offsetWidth / 2);
	var q = myRange.value;
    myValue.innerHTML = '$ '+ parseInt(q).format(2) + ' ' + myUnits;
    myValue.parentElement.style.left = px + 'px';
  };
  
 Number.prototype.format = function(n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};
</script>
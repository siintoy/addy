<ul class="nav navbar-nav side-nav">
<?php /* 
	<li>
		<i class="fa fa-fw">
			<div style="padding:5px;text-align:left;border-bottom:1px solid;width:225px;">
				<div style="padding-bottom:5px"><img style="width:60px;height:50px;" src="<?php echo base_url();?>img/unknown.gif"/></div>
				<b style="color:white;">Welcome, <?php echo ucwords($this->session->userdata("username")); ?></b> <br>
				<a href="#">Change Password</a> | <a href="#">Edit Details</a>
			</div>
		</i> 
	</li>
	
	
	<li>
		<a  target="_blank"  href="<?php echo site_url();?>"><i class="fa fa-fw fa-dashboard"></i> Home</a>
	</li>*/ ?>
	<?php if($this->session->userdata("role") == 0): ?>
	<li>
		<a href="<?php echo site_url("admin/main");?>"><i class="glyphicon glyphicon-bullhorn"></i> News & Announcements</a>
	</li>
	<?php endif; ?>
	<?php if($this->session->userdata("role") == 0): ?>
	<li>
		<a data-target="#agency" data-toggle="" href="javascript:;"><i class="glyphicon glyphicon-th-list"></i> Agency </a>
		<ul class="" id="agency">
			<li>
				<a href="<?php echo base_url();?>index.php/admin/agency/new_agency"><i class="glyphicon glyphicon-minus"></i> Add New Agency</a>
			</li>
			<li>
				<a href="<?php echo base_url();?>index.php/admin/agency/agency_list"><i class="glyphicon glyphicon-minus"></i> View List</a>
			</li>
		</ul>
	</li>
	<?php endif; ?>

	<?php if($this->session->userdata("role") == 0): ?>
		<li>
			<a data-target="#recruiter" data-toggle="" href="javascript:;"><i class="glyphicon glyphicon-asterisk"></i> Recruiter </a>
			<ul class="" id="recruiter">
				<li>
					<a href="<?php echo base_url();?>index.php/admin/recruiter/new_recruiter"><i class="glyphicon glyphicon-minus"></i>  Add New Recruiter</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>index.php/admin/recruiter/recruiter_list"><i class="glyphicon glyphicon-minus"></i> View List</a>
				</li>
			</ul>
		</li>
	<?php endif; ?>
	
	<?php if($this->session->userdata("role") == 1 OR $this->session->userdata("role") == 0): ?>
		<li>
			<a data-target="#profile" data-toggle="" href="javascript:;"><i class="glyphicon glyphicon-user"></i> Profile </a>
			<ul class="" id="profile">
				<li>
					<a href="<?php echo base_url();?>index.php/admin/profile/new_profile"><i class="glyphicon glyphicon-minus"></i> Add New Profile</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>index.php/admin/profile/profiles"><i class="glyphicon glyphicon-minus"></i> View List</a>
				</li>
			</ul>
		</li>
	<?php endif; ?>
	
	<?php if($this->session->userdata("role") == 2): ?>
		<li><a href="<?php echo site_url("admin/main");?>"><i class="glyphicon glyphicon-bullhorn"></i> FAQs</a></li>
	<?php endif; ?>
</ul>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('isLoggedin')) redirect("login");
		
		#if($this->session->userdata("role") <> 0)redirect("main");
	}
	
	public function index($data = array())
	{
		$var1  = array();
		$content  = array();
		$content['pageHeader'] 	= "News & Announcements";
		
		if($this->input->post()){
			$news = $this->input->post("news");
			
			$this->db->where("id", 1);
			$this->db->update("news", array("news" => $news));
		}
		
		$content['news'] = $this->db->get("news")->result();
		$data['sidemenu'] = $this->load->view("admin/sidemenu", $var1, true);
		
		switch($this->session->userdata("role")){
			
			case 1:
				$data['content'] 	= $this->load->view("admin/agency", $content, true);
				break;
			case 2:
				$data['content'] 	= $this->load->view("admin/agency", $content, true);
				break;
			default:
				$data['content'] 	= $this->load->view("admin/news", $content, true);
			break;
		}
		
		$this->load->view('admin/index', $data);
	}
	
}
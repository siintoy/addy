/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.13-MariaDB : Database - profilexchange
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`profilexchange` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `profilexchange`;

/*Table structure for table `agency` */

DROP TABLE IF EXISTS `agency`;

CREATE TABLE `agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `address` text,
  `phone` varchar(11) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `contact_person` varchar(32) DEFAULT NULL,
  `contact_person_no` varchar(11) DEFAULT NULL,
  `contact_person_email` varchar(32) DEFAULT NULL,
  `interview` varchar(12) DEFAULT NULL,
  `bus_reg_no` varchar(11) DEFAULT NULL,
  `url` text,
  `bank_account_no` varchar(30) DEFAULT NULL,
  `bank_account_name` varbinary(32) DEFAULT NULL,
  `no_listing_allowed` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `agency` */

insert  into `agency`(`id`,`userid`,`name`,`address`,`phone`,`country`,`contact_person`,`contact_person_no`,`contact_person_email`,`interview`,`bus_reg_no`,`url`,`bank_account_no`,`bank_account_name`,`no_listing_allowed`,`status`) values (1,1,'Agency Name','Agency Address','Agency Phon','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(2,2,'Agency Name','Agency Address','Agency Phon','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(3,3,'Agency Name','Agency Address','Agency Phon','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(4,4,'Agency Name','Agency Address','Agency Phon','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Agency Busi','Agency URL','Bank Account No. ','Bank Account Name ',0,'active'),(7,7,'My new Agency','04c Kampupot St. Puwang Wawa, Taguig City','0912212121','one','Jan Christian','09468965897','christianaristain@gmail.com','15872585','5458774','http://localhost/addy/index.php/admin/agency/new_agency','55555555','bane name',12,'active');

/*Table structure for table `recruiter` */

DROP TABLE IF EXISTS `recruiter`;

CREATE TABLE `recruiter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `address` text,
  `phone` varchar(11) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `contact_person` varchar(32) DEFAULT NULL,
  `contact_person_no` varchar(11) DEFAULT NULL,
  `contact_person_email` varchar(32) DEFAULT NULL,
  `interview` varchar(12) DEFAULT NULL,
  `bus_reg_no` varchar(11) DEFAULT NULL,
  `url` text,
  `bank_account_no` varchar(30) DEFAULT NULL,
  `bank_account_name` varbinary(32) DEFAULT NULL,
  `no_booking_allowed` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`,`username`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `recruiter` */

insert  into `recruiter`(`id`,`username`,`password`,`name`,`address`,`phone`,`country`,`contact_person`,`contact_person_no`,`contact_person_email`,`interview`,`bus_reg_no`,`url`,`bank_account_no`,`bank_account_name`,`no_booking_allowed`,`status`) values (1,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(2,'12','121','1','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(3,'Username','1','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(4,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(5,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(6,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`user_type`,`status`) values (1,'Username','dc647eb65e6711e155375218212b3964',2,1),(2,'Usernamed','dc647eb65e6711e155375218212b3964',2,1),(3,'Usernames','dc647eb65e6711e155375218212b3964',2,1),(4,'Usernamedasdas','dc647eb65e6711e155375218212b3964',2,1),(5,'Usernamedasd','dc647eb65e6711e155375218212b3964',2,1),(6,'haha','dc647eb65e6711e155375218212b3964',2,1),(7,'aeonflux','7cf253e55693c11aacd6203042d8b19d',2,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

<?php if($flash = $this->session->flashdata('flsh_msg')): ?>
  <div class="<?php echo $flash['class']; ?>">
    <a href="#" class="close" data-dismiss="<?php echo $flash['dismiss']; ?>" aria-label="close">&times;</a>
    <strong><?php echo $flash['errType']; ?>!: </strong> <?php echo $flash['msg']; ?>.
  </div> 
<?php endif; ?>
<div style="padding:10px;" class="row">
	<div class="">
	
		<div class="dataTable_wrapper">
	<?php if(!empty($data)): ?>
		<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
		  <tr>
			<th>Name</th>
			<th>Date of Birth</th>
			<th>Nationality</th>
			<th>Marital Status</th>
			<th>Age</th>
			<th>Status</th>
		  </tr>
		</thead>
		<tbody>
		<?php foreach($data->result() as $rows): ?>
		  <tr>
			<td><?php echo $rows->name; ?></td>
			<td><?php echo $rows->date_of_birth; ?></td>
			<td><?php echo $rows->nationality; ?></td>
			<td><?php echo ucwords(strtolower($rows->marital_status)); ?></td>
			<td><?php echo $rows->age; ?></td>
			<td>
				<center>
					<a href="<?php echo site_url('admin/agency/view_profile?agency_id='.$id.'&profile_id='.$rows->id.''); ?>">View</a>  | 
					<a href="<?php echo site_url('admin/agency/publish?agency_id='.$id.'&profile_id='.$rows->id.'&value='.($rows->isPublish == 1 ? 0 : 1).''); ?>"><?php echo ($rows->isPublish == 1) ? "Unpublish Profile" : "Publish Profile"; ?></a>
				</center>
			</td>

		  </tr>
		<?php endforeach; ?>
		</tbody>
	  </table>
	 <?php endif; ?>
	</div>
</div>	
</div>	

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure want to delete <span id="delUsername"></span> ?.</p>
        </div>
        <div class="modal-footer">
		<form action="<?php echo site_url("admin/agency/delete");?>" method="post">
			<button type="submit" name="delete" id="delete" value="" class="btn btn-primary">Delete</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</form>
        </div>
      </div>
      
    </div>
  </div>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <script>
	$(document).ready(function() {
		$("#delete").click(function() {
			val = $(this).val();
		});
	});
	function deleteUser(username, userid){
		$("#delUsername").html(username);
		$("#delete").val(userid);
	}
 </script>
  
<div class="form-group">
	<label for="allergies">Allergies (if any):</label>
	<input required type="text"  placeholder="Allergies (if any)"  class="form-control" id="allergies" name="personal_medical[allergies]" />
</div>

<div class="form-group">
	<label>Past and existing illnesses (including chronic ailments and illnesses requiring medication):</label> <br />
	<?php $x = 0; foreach($illness as $q): ?>
	<label><input name="personal_medical[illness][]" type="checkbox" value="<?php echo $q; ?>"> <?php echo $q; ?> </label><br />
	<?php $x++; endforeach; ?>
</div>

<div class="form-group">
	<label for="disabilities">Physical disablilities:</label>
	<textarea required type="text"  placeholder="Physical disablilities"  class="form-control" id="disabilities" name="personal_medical[disabilities]" ></textarea>
</div>

<div class="form-group">
	<label for="dietary_restrictions">Dietary restrictions:</label>
	<textarea required type="text"  placeholder="Dietary restrictions"  class="form-control" id="dietary_restrictions" name="personal_medical[dietary_restrictions]" ></textarea>
</div>

<div class="form-group">
	<label for="food_preferences">Food handling preferences:</label>
	<select  class="form-control"  name="personal_medical[food_preferences]" id="food_preferences">
		<?php foreach($food as $w): ?>
			<option  value="<?php echo $w; ?>"><?php echo $w; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="rest_day">Preference for rest day per month:</label>
	<input required type="text" onkeypress='return isNumberKey(event);' placeholder="Preference for rest day per month"  class="form-control" id="rest_day" name="personal_medical[rest_day]" />
</div>

<div class="form-group">
	<label for="others">Any other remarks:</label>
	<textarea required type="text"  placeholder="Any other remarks"  class="form-control" id="others" name="personal_medical[others]" ></textarea>
</div>

<div class="form-group">
	<label>Please indicate the method(s) used to evaluate the FDW's skills:</label> <br />
	<?php $x = 0; foreach($eval as $e): ?>
	<label><input name="personal_medical[evaluation_skill][]" type="checkbox" value="<?php echo $e; ?>"> <?php echo $e; ?> </label><br />
	<?php $x++; endforeach; ?>
</div>
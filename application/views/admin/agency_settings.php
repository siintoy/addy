<?php if(isset($submit)): ?>
  <div class="<?php echo $class; ?>">
    <a href="#" class="close" data-dismiss="<?php echo $dismiss; ?>" aria-label="close">&times;</a>
    <strong><?php echo $errType; ?>!: </strong> <?php echo $msg; ?>.
  </div> 
<?php endif; ?>
<form role="form" enctype="multipart/form-data" method="post" action="<?php echo base_url();?>index.php/admin/agency/settings?id=<?php echo $id; ?>">
	<div class="form-group">
		<label for="license">License Number:</label>
		<input required type="text" value="<?php echo $data[0]->serial_no; ?>"  placeholder="License Number"  class="form-control" id="license" name="license">
	</div>
<div class="form-group">
<label class="btn btn-primary" for="my-file-selector">
    <input id="my-file-selector" type="file" style="display:none;" name="logo" >
    Upload Agency Logo 
</label>
<span class='label label-info' id="upload-file-info"></span> <br> <br>
<div style="width:100%"><img id="logo_output"/></div>

</div>
<div class="form-group">

<label class="btn btn-primary" for="my-file-selector1">
    <input id="my-file-selector1" type="file" style="display:none;" name="banner" onchange="$('#upload-file-info1').html($(this).val());">
    Upload Banner 
</label>
<span class='label label-info' id="upload-file-info1"></span> <br> <br>
<div style="width:100%"><img id="banner_output"/></div>

</div>
	<div class="form-group">
		<input type="submit" class="btn btn-primary"  value="Submit" />
	</div>
</form>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
	
$("#my-file-selector").change(function(evt){
	loadFile(evt, "logo_output");
	console.log($(this).val());
	$('#upload-file-info').html($(this).val());
});

$("#my-file-selector1").change(function(evt){
	loadFile(evt, "banner_output");
	$('#upload-file-info1').html($(this).val());
});


  var loadFile = function(event, id) {
    var output = document.getElementById(id);
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
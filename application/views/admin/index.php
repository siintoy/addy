<html lang="en"><head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
	<?php 
	
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		
		header('Cache-Control: no-store, no-cache, must-revalidate');
		
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		
		header('Pragma: no-cache');
	?>
    <title>HIRO</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/sb-admin.css">

    <!-- Custom Fonts -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
    <!-- DataTables CSS -->
    <link href="<?php echo base_url(); ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_url(); ?>bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

	
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body">

    <div id="wrapper">

        <!-- Navigation -->
        <nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo site_url("main?id=".$this->session->userdata("userid").""); ?>" class="navbar-brand"><img style="margin-top:-4px;" width="85px;" height="30px" src="<?php echo base_url();?>uploads/<?php echo ($this->session->userdata("logo")) ? $this->session->userdata("logo") : "your-logo-here.gif"; ?>"/></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> <?php echo $this->session->userdata("username"); ?> <b class="caret"></b></a>
                    <ul style="width:210px;padding:10px;" class="dropdown-menu">
						<li>
							<div style="padding-bottom:5px">
								<img style="width:60px;height:50px;" src="<?php echo base_url();?>img/unknown.gif"/>
									Welcome, <?php echo ucwords($this->session->userdata("username")); ?>
								<div>
									<?php if($this->session->userdata("role") == 0): ?><a href="#">Change Password</a> <br> <?php endif; ?>
									<a href="#">Edit Details</a>
								</div>
							</div>
						</li>
						<li class="divider"></li>
                        <li>
                            <a href="<?php echo site_url("logout"); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php echo (isset($sidemenu) ? $sidemenu : false);?>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo (isset($pageHeader) ? $pageHeader : false);?>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
				
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-body">
								<?php echo (isset($content) ? $content : false);?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

       <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>


</body>
</html>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
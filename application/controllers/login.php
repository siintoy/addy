<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct(){
		parent::__construct();
		if($this->session->userdata('isLoggedin')) redirect("main");
	}
	 
	public function index($data = array())
	{
		
		if($this->input->post()){
			$strMsg 		= "";
			$username 	= trim($this->input->post("username"));
			$password 	= trim($this->input->post("password"));
			
			$arrData 	= array(
									'username' => $username, 
									'password' => $password
									);
			
			$arrResult 	= $this->Mod_login->login($arrData);
			
			if(count($arrResult) > 0){
			
				$intUserID 		= $arrResult[0]->id;
				$intUserType 	= $arrResult[0]->user_type;
				$arrSession = array(
										"userid" 	=> $intUserID,
										"username" 	=> $username,
										"role"		=> $intUserType
									);
				if($intUserType > 0){
					$arrResultValidate = $this->Mod_login->validate($intUserID, $intUserType);
					if(count($arrResultValidate) > 0){
						$strMsg = "Unable to login your account has been ".$arrResultValidate[0]->status." please contact the site admin. ";
					}else{
						$table = array("admin", "recruiter", "agency");
						
						if($table[$intUserType] == "recruiter"){
							$this->Mod_login->create_session($arrSession);
							redirect("/admin/profile/new_profile?id={$intUserID}");
						}else if($table[$intUserType] == "agency"){
							$settings = $this->Mod_profile->get_settings($intUserID);
							$arrSession['logo'] = $settings[0]->logo;
							$this->Mod_login->create_session($arrSession);
							redirect("admin/agency?id={$intUserID}");
						}else{
							redirect("admin");
						}
						#redirect("admin/".$table[$intUserType]."/".$table[$intUserType]."_list");
					}
				}else{
					$this->Mod_login->create_session($arrSession);
					redirect("admin");
				}
			}else{
				$strMsg = "Invalid Credentials";
			}
			
			$data['msg'] 			= $strMsg;
			$data['username'] 	= $username;
			$data['password'] 	= $password;
		}
		$content['body'] = $this->load->view("login", $data, true);
		$this->load->view('layout', $content);
	}
}

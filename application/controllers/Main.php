<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct(){
		parent::__construct();
		#if(!$this->session->userdata('isLoggedin')) redirect("login");
	}
	 
	public function index()
	{
		if($this->input->get("id")){
			$id = $this->input->get("id");
		
			$var1 = array();
			#$data['menu'] = $this->load->view("menu", $var1, true);
			$data = array('id' => $id);
			$var1['code'] 		= $this->Mod_profile->getByCountry($data);
			$var1['latest']		= $this->Mod_profile->getLatestProfile($id);
			$var1['title'] 		= $this->Mod_profile->getAgencyName($id);
			$var1['settings'] 	= $this->Mod_profile->get_settings($id);
			$var1['id'] 			= $id;
			$var1['header'] 	= true;
			$var1['rel']			= array('CHRISTIANITY', 'ISLAM', 'HINDUISM', 'TAOISM', 'BUDDHISM', 'SHINTO', 'SIKHISM', 'JUDAISM', 'AGNOSTIC', 'ATHEIST');
			$var1['educ']		= array('NONE', 'COMPLETED PRIMARY SCHOOL', 'COMPLETED', 'SECONDARY SCHOOL', 'DIPLOMA', 'DEGREE', 'PHD', 'CERTIFICATE OF COMPLETION');
			$var1['stat']		= array('SINGLE', 'IN A RELATIONSHIP', 'ENGAGED', 'MARRIED', 'DIVORCED', 'WIDOWED');
			$var1['illness']		= array('Mental illness', 'Tuberculosis', 'Epilepsy', 'Heart disease', 'Asthma', 'Malaria', 'Diabetes', 'Operations', 'Hypertension', 'Others');
			$var1['food']		= array('No pork', 'No beef', 'Vegetarian', 'Others');
			$var1['eval']		= array('Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA', 
														'Interviewed by Singapore EA', 'Interviewed by overseas training centre/EA');

			if(count($var1['settings']) > 0){
				$var1['body_content'] = $this->load->view("home", $var1, true);
				$data['body'] = $this->load->view("body", $var1, true);
				
				
				$this->load->view('layout',$data);
			}else{
				 show_404(); 
			}
		}
		else{
			show_404();
		}
	}
	
	public function faqs(){
		if($this->input->get("id")){
			$id = $this->input->get("id");
			$var1 = array();
			#$data['menu'] = $this->load->view("menu", $var1, true);
			$data = array('id' => $id);
			$var1['code'] = $this->Mod_profile->getByCountry($data);
			$var1['latest']	= $this->Mod_profile->getLatestProfile($id);
			$var1['settings'] = $this->Mod_profile->get_settings($id);
			$var1['faqs'] = $this->Mod_profile->get_faqs($id);
			$var1['id'] = $id;
			if(count($var1['settings']) > 0){
				$var1['body_content'] = $this->load->view("faqs", $var1, true);
				$data['body'] = $this->load->view("body", $var1, true);
				$this->load->view('layout',$data);
			}else{
				 show_404(); 
			}
		}
		else{
			show_404();
		}
	}
	
	public function search_profiles(){
		if($this->input->get("id")){
			$id = $this->input->get("id");
		
			$var1 = array();
			#$data['menu'] = $this->load->view("menu", $var1, true);
			$data = array('id' => $id);
			$var1['code'] = $this->Mod_profile->getByCountry($data);
			$var1['settings'] = $this->Mod_profile->get_settings($id);
			$var1['id'] = $id;
			$var1['header'] = true;

			if(count($var1['settings']) > 0){
			
				if($this->input->post("search")){
					$post = $this->input->post();
				
						$age 		= explode("-", $this->input->post("age"));
						
						if($age[0] == "40+"){
							$age[0] = 40;
							$age[1] = 100;
						}
						$search['education'] 		= $post['education'];
						$search['religion']  		= $post['religion'];
						$search['marital_status'] 	= $post['marital_status'];
						
					$var1['profile'] = $this->Mod_profile->get_search_profiles($search, $age, $id);
					$var1['body_content'] = $this->load->view("search_profiles", $var1, true);
					$data['body'] = $this->load->view("body", $var1, true);
				}
				
				$this->load->view('layout',$data);
			}else{
				 show_404(); 
			}
		}
		else{
			show_404();
		}
	}

	public function advance_search(){
		if($this->input->get("id")){
			$id = $this->input->get("id");
		
			$var1 = array();
			#$data['menu'] = $this->load->view("menu", $var1, true);
			$data = array('id' => $id);
			$var1['code'] = $this->Mod_profile->getByCountry($data);
			$var1['latest']	= $this->Mod_profile->getLatestProfile($id);
			$var1['settings'] = $this->Mod_profile->get_settings($id);
			$var1['id'] = $id;
			$var1['header'] = true;
			$var1['rel']	= array('CHRISTIANITY', 'ISLAM', 'HINDUISM', 'TAOISM', 'BUDDHISM', 'SHINTO', 'SIKHISM', 'JUDAISM', 'AGNOSTIC', 'ATHEIST');
			$var1['educ']	= array('NONE', 'COMPLETED PRIMARY SCHOOL', 'COMPLETED', 'SECONDARY SCHOOL', 'DIPLOMA', 'DEGREE', 'PHD', 'CERTIFICATE OF COMPLETION');
			$var1['stat']	= array('SINGLE', 'IN A RELATIONSHIP', 'ENGAGED', 'MARRIED', 'DIVORCED', 'WIDOWED');
			$var1['illness']= array('Mental illness', 'Tuberculosis', 'Epilepsy', 'Heart disease', 'Asthma', 'Malaria', 'Diabetes', 'Operations', 'Hypertension', 'Others');
			$var1['food']	= array('No pork', 'No beef', 'Vegetarian', 'Others');
			$var1['eval']	= array('Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA', 
														'Interviewed by Singapore EA', 'Interviewed by overseas training centre/EA');
			$var1['nationality'] = $this->Mod_profile->getNationality($id);
			$var1['country'] = $this->Mod_profile->getCountries($id);
			$var1['lang'] =  array("English","Mandarin/Chinese-Dialect","Basaha Indonesia/Malayia","Tamil","Malayan","Telugu","Burmese","Tagalog","Thai","Hindi");
 			
			if(count($var1['settings']) > 0){
				$var1['body_content'] = $this->load->view("advance_search", $var1, true);
				$data['body'] = $this->load->view("body", $var1, true);
				
				
				$this->load->view('layout',$data);
			}else{
				 show_404(); 
			}
		}
		else{
			show_404();
		}
	}
	
	public function results(){
		if($this->input->get("id")){
			$id = $this->input->get("id");
			$var1 = array();
			#$data['menu'] = $this->load->view("menu", $var1, true);
			$data = array('id' => $id);
			$var1['code'] = $this->Mod_profile->getByCountry($data);
			$var1['latest']	= $this->Mod_profile->getLatestProfile($id);
			$var1['settings'] = $this->Mod_profile->get_settings($id);
			$var1['id'] = $id;
			$var1['header'] = true;
 			
			if(count($var1['settings']) > 0){
				$data['body'] = $this->load->view("body", $var1, true);
				
				if($this->input->post()){
					$post 			= $this->input->post();
					$age 			= $post['age'];
					$status 		= $post['status'];
					$nationality 	= $post['nationality'];
					$country 		= $post['country'];
					$religion 		= $post['religion'];
					$education 		= $post['education'];
					
					$language 		= $post['language'];
					
					
					$strN 			= "";
					$strS 			= "";
					$strC 			= "";
					$strR 			= "";
					$strE 			= "";
					$strL 			= "";
					
					if($nationality[0] <> "0"){
						$strN	= "AND LOWER(nationality) IN(";
						
						foreach($nationality as $strNX){
							$strN .= "'$strNX',";
						}
						$strN = substr($strN, 0, -1).")";	
					}
				
					if($status[0] <> "0"){
						$strS 			= "AND marital_status IN(";
						foreach($status as $strSX){
							$strS .= "'$strSX',";
						}
						$strS = substr($strS, 0, -1).")";
					}
					
					if($country[0] <> "0"){
						$strC 		= "AND country IN(";
						foreach($country as $strCX){
							$strC .= "'$strCX',";
						}
						$strC = substr($strC, 0, -1).")";
					}
					
					if($religion[0] <> "0"){
						$strR  	= "AND religion IN(";
						foreach($religion as $strRX){
							$strR .= "'$strRX',";
						}
						$strR = substr($strR, 0, -1).")";
					}
					
					if($education[0] <> "0"){
						$strE 			= "AND education IN(";
						foreach($education as $strEX){
							$strE .= "'$strEX',";
						}
						$strE = substr($strE, 0, -1).")";
					}
					
					if($language[0] <> "0"){
						$strL 	 = "AND language IN(";
						foreach($language as $strLX){
							$strL .= "'$strLX',";
						}
						$strL = substr($strL, 0, -1).")";
					}
						$arrAge = "";
						$strAge = "";
						$intCounter = 0;
						$intCount   = count($age);
						
						if($intCount > 0){
							
							if($age[0] <> "0"){
								$arrTemp1 = array();
								$arrTemp2 = array();
								$firstIndex = $age[0];
								$lastIndex  = $age[($intCount - 1)];
								
								if($firstIndex <> "40+"){
									$arrTemp1 = explode("-", $firstIndex);
								}else{
									$arrTemp1[0] = 40;
								}
							
								if($lastIndex <> "40+"){
									$arrTemp2 = explode("-", $lastIndex);
								}else{
									$arrTemp2[1] = "100";
								}
								$strAge .= "AND age BETWEEN ".$arrTemp1[0]." AND ".$arrTemp2[1]."  ";
							}

						}
						
					$sql = $strAge . $strS . $strN . $strC . $strR . $strE . $strL . " ";
					$var1['profile'] = $this->Mod_profile->search_results($sql, $this->input->get("id"));
					$var1['body_content'] = $this->load->view("search_profiles", $var1, true);
				}else{
					redirect("main/advance_search?id={$id}");
				}
				$data['body'] = $this->load->view("body", $var1, true);
				$this->load->view('layout',$data);
			}else{
				 show_404(); 
			}
		}
		else{
			show_404();
		}
	}
	
	public function view_profile($profile_id = 0){
		if($this->input->get("id")){
			$id = $this->input->get("id");
			$profile_id = $this->input->get("profile_id");
		
			$var1 = array();
			$data = array('id' => $id);
			$var1['code'] 		= $this->Mod_profile->getByCountry($data);
			$var1['latest']		= $this->Mod_profile->getLatestProfile($id);
			$var1['settings'] 	= $this->Mod_profile->get_settings($id);
			$var1['id'] 		= $id;
			#$var1['header'] 	= true;
			if(count($var1['settings']) > 0){
				$where = " AND info.id = {$profile_id}";
				$var1['result'] = $this->Mod_profile->search_results($where, $id);
				$var1['body_content'] = $this->load->view("view_profile", $var1, true);
				$data['body'] = $this->load->view("body", $var1, true);

				$this->load->view('layout',$data);
			}else{
				 show_404(); 
			}
		}
		else{
			show_404();
		}
	}
	
}

<div class="container">
	<div style="padding:5px;border:1px solid #ccc;  "class="col-xs-12">
		<h4>Advance Search</h4>
		<div class="form-group">
			<form role="form" method="POST" action="<?php echo base_url();?>index.php/main/results?id=<?php echo $id;?>">
				<div style="padding:5px;"class="col-xs-12">
					<table class="table table-nonfluid">
						<tr>
							<td></td>
							<td><input type="submit" name="search" class="btn btn-success btn-block btn-lg" value="Search Profile" /></td>
							<td></td>
						</tr>
						<tr>
							<td class="col-md-3">
								<label>Nationality:</label> <br>
								<?php foreach($nationality as $nations): ?>
								<label><input name="nationality[]" class="nationality" type="checkbox" value="<?php echo $nations->nationality; ?>"> <?php echo ucwords(strtolower($nations->nationality)); ?> </label><br />
								<?php endforeach; ?>
								<label><input name="nationality[]" class="nationality nationalityx" type="checkbox" value="0"> No Preference</label><br />
							</td>
							<td class="col-md-3">
								<label>Country:</label> <br>
								<?php foreach($country as $counties): ?>
								<label><input name="country[]" class="country" type="checkbox" value="<?php echo $counties->code; ?>"> <?php echo $counties->name; ?> </label><br />
								<?php endforeach; ?>
								<label><input name="country[]" class="country countryx" type="checkbox" value="0"> No Preference</label><br />
							</td>
							<td class="col-md-3">
								<label>Religion:</label> <br>
								<?php foreach($rel as $rels): ?>
								<label><input name="religion[]" class="religion" type="checkbox" value="<?php echo $rels; ?>"> <?php echo ucwords(strtolower($rels)); ?> </label><br />
								<?php endforeach; ?>
								<label><input name="religion[]" class="religion religionx" type="checkbox" value="0"> No Preference</label><br />
							</td>
						</tr>
						<tr>
							<td class="col-md-3">
								<label for="age">Age:</label> <br>
								<?php for($x = 17;$x <= 40;$x=($x + 5)): ?>
								<label><input name="age[]" class="age" type="checkbox" value="<?php echo (($x + 5) < 40 ? $x."-".($x + 5) : "40+"); ?>"> <?php echo (($x + 5) < 40 ? $x." - ".($x + 5) : "Above 40"); ?></label><br />
								<?php endfor; ?>
								<label><input name="age[]" class="age agex" type="checkbox" value="0"> No Preference</label><br />
							</td>
							<td class="col-md-3">
								<label>Education:</label> <br>
								<?php foreach($educ as $educs): ?>
								<label><input name="education[]" class="education" type="checkbox" value="<?php echo $educs; ?>"> <?php echo ucwords(strtolower($educs)); ?> </label><br />
								<?php endforeach; ?>
								<label><input name="education[]" class="education educationx" type="checkbox" value="0"> No Preference</label><br />
							</td>
							<td class="col-md-3">
								<label>Marital Status:</label> <br>
								<?php foreach($stat as $stats): ?>
								<label><input name="status[]" class="status" type="checkbox" value="<?php echo $stats; ?>"> <?php echo ucwords(strtolower($stats)); ?> </label><br />
								<?php endforeach; ?>
								<label><input name="status[]" class="status statusx" type="checkbox" value="0"> No Preference</label><br />
							</td>
						</tr>
						<tr>
							<td class="col-md-3">
								<label>Language:</label> <br>
								<?php foreach($lang as $language): ?>
								<label><input name="language[]" class="language" type="checkbox" value="<?php echo $language; ?>"> <?php echo ucwords(strtolower($language)); ?> </label><br />
								<?php endforeach; ?>
								<label><input name="language[]" class="language languagex" type="checkbox" value="0"> No Preference</label><br />
							</td>
							<td class="col-md-3">
								<label>Others:</label> <br>
								<label><input name="others[]" class="video" type="checkbox" value="1"> With Video </label><br />
								<label><input name="others[]" class="video" type="checkbox" value="2"> Has Children </label><br />
								<label><input name="others[]" class="video videox" type="checkbox" value="0"> No Preference</label><br />
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="search" class="btn btn-success btn-block btn-lg" value="Search Profile" /></td>
							<td></td>
						</tr>
					</table>
				</div>
			</form>
		</div>
		
	</div>

</div>
<br />
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
$("document").ready(function(x, y){
	$("input[type=checkbox]").each(function(){
		//console.log();
		val = $(this).val();
		if(val == "0"){
			$(this).attr("checked", true);
		}
	});
	
	$("input[type=checkbox]").change(function(){
		val = $(this).val();
		name = $(this).attr("name");
		eclass = $(this).attr("class");
		myclass = eclass.split(" ");
	
		if(val == "0"){
			//console.log();
			$("[name='"+name+"']").prop("checked", false);
			$("."+myclass[1]).prop("checked", true);
		}else{
			$("."+myclass+"x").prop("checked", false);
		}
	});
	
});
</script>
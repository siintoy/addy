<?php $disabled = $this->session->userdata("role") > 0 ? " disabled " : false; ?>
<div style="padding:10px;" class="row">
<?php if($flash = $this->session->flashdata('flsh_msg')): ?>
  <div class="<?php echo $flash['class']; ?>">
    <a href="#" class="close" data-dismiss="<?php echo $flash['dismiss']; ?>" aria-label="close">&times;</a>
    <strong><?php echo $flash['errType']; ?>!: </strong> <?php echo $flash['msg']; ?>.
  </div> 
<?php endif; ?>
  <!-- Modal content-->
  <?php foreach($data as $rows): ?>
    <div class="">
      <div class="">
         <form role="form" method="post" action="<?php echo base_url();?>index.php/admin/recruiter/view?id=<?php echo $this->input->get("id"); ?>&username=<?php  echo $this->input->get("username"); ?>">
		  <div class="form-group">
			<label for="name">Recruiter Name:</label>
			<input value="<?php echo $rows->name; ?>" required type="text" placeholder="Recruiter Name"  class="form-control" id="name" name="credentials[name]">
		  </div>
		  <div class="form-group">
			<label for="address">Recruiter Address:</label>
			<input value="<?php echo $rows->address; ?>" required type="text" placeholder="Recruiter Address"  class="form-control" id="address" name="credentials[address]"/>
		  </div>
		  <div class="form-group">
			<label for="phone">Recruiter Phone:</label>
			<input value="<?php echo $rows->phone; ?>" required type="text" placeholder="Recruiter Phone"  class="form-control" id="phone" name="credentials[phone]">
		  </div>
		  <div class="form-group">
			<label for="country">Country:</label>
			<select required name="credentials[country]" id="country" class="form-control">
				<?php foreach($country as $countries): ?>
				<option  <?php echo ($rows->country == $countries->code ? " selected " : false); ?> value="<?php echo $countries->code; ?>"><?php echo $countries->name; ?></option>
				<?php endforeach; ?>
			</select>
			</div>
		  <div class="form-group">
			<label for="contact_person">Contact Person:</label>
			<input  value="<?php echo $rows->contact_person; ?>" required type="text" class="form-control" placeholder="Contact Person"   id="contact_person" name="credentials[contact_person]">
		  </div>
		  <div class="form-group">
			<label for="contact_person_no">Contact Person No.:</label>
			<input  value="<?php echo $rows->contact_person_no; ?>" required type="text" class="form-control" placeholder="Contact Person No."  id="contact_person_no" name="credentials[contact_person_no]">
		  </div>
		  <div class="form-group">
			<label for="contact_person_email">Contact Person Email:</label>
			<input  value="<?php echo $rows->contact_person_email; ?>" required type="email" class="form-control" placeholder="Contact Person Email" id="contact_person_email" name="credentials[contact_person_email]">
		  </div>
		  <div class="form-group">
			<label for="interview">Available Form of Interview: (Phone / Skype)</label>
			<input <?php echo $disabled; ?> value="<?php echo $rows->interview; ?>" required type="text" placeholder=" Interview Phone Number / Skyper Username " class="form-control" id="interview" name="credentials[interview]"/>
			</div>
		  <div class="form-group">
			<label for="bus_reg_no">Recruiter Business Registration No.:</label>
			<input <?php echo $disabled; ?> value="<?php echo $rows->bus_reg_no; ?>" required type="text" placeholder="Recruiter Business Registration No." class="form-control" id="bus_reg_no" name="credentials[bus_reg_no]">
			</div>
		  <div class="form-group">
			<label for="url">Recruiter URL:</label>
			<input <?php echo $disabled; ?> value="<?php echo $rows->url; ?>" required type="text" placeholder="Recruiter URL" class="form-control" id="url" name="credentials[url]">
			</div>
		  <div class="form-group">
			<label for="bank_account_no">Bank Account No. :</label>
			<input value="<?php echo $rows->bank_account_no; ?>" required type="text" placeholder="Bank Account No. " class="form-control" id="bank_account_no" name="credentials[bank_account_no]">
			</div>
		  <div class="form-group">
			<label for="bank_account_name">Bank Account Name :</label>
			<input value="<?php echo $rows->bank_account_name; ?>" required type="text" placeholder="Bank Account Name " class="form-control" id="bank_account_name" name="credentials[bank_account_name]">
			</div>
		  <div class="form-group">
			<label for="no_booking_allowed">Number Booking Allowed :</label>
			<input <?php echo $disabled; ?> value="<?php echo $rows->no_booking_allowed; ?>" required type="text" placeholder="Number Booking Allowed " class="form-control" id="no_booking_allowed" name="credentials[no_booking_allowed]">
			</div>
			 <div class="form-group">
			  <label for="status">Status:</label>
			  <select <?php echo $disabled; ?> required class="form-control" id="status" name="credentials[status]">
				<option <?php echo $rows->status == "active" ? " selected " : false; ?> value="active">Active</option>
				<option <?php echo $rows->status == "inactive" ? " selected " : false; ?> value="inactive">Inactive</option>
				<option <?php echo $rows->status == "suspended" ? " selected " : false; ?> value="suspended">Suspended</option>
			  </select>
			</div>
			<input type="submit" class="btn btn-primary"  value="Submit" />
		</form>
      </div>
    </div>
<?php endforeach; ?>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	protected $userid; 
	
	function __construct(){
		parent::__construct();
		
		ini_set('upload_max_filesize', '1G');
		ini_set('post_max_size', '1G');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 300);
		
		if(!$this->session->userdata('isLoggedin')) redirect("login");
		$this->userid = $this->session->userdata("userid");
		
		
	}
	 
	public function index($data = array())
	{
		$var1  = array();
		$data['sidemenu'] = $this->load->view("admin/sidemenu", $var1, true);
		$this->load->view('admin/index', $data);
	}
	 
	private function do_upload($files = array()){
		$arrFile = array();
		foreach ($files as $index => $value)
        {
            if ($value['name'] != '')
            {
				$config = array();
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
                $this->load->library('upload');
                $this->upload->initialize($config);

                //upload the image
                if ($this->upload->do_upload($index))
                {
					$data[$index] = $this->upload->data();
					if($index == "picture"){
						$arrFile[$index] = array($data[$index]['file_name']);
					}else if($index == "video"){
						$arrFile[$index] = array($data[$index]['file_name']);
					}
                }
            }
        }
		
		return $arrFile;
	} 
	 
	public function new_profile($data = array())
	{
		$var1  = array();
		$content = array();
		
		$content['pageHeader'] = "New Profile";
		$info['rel']					= array('CHRISTIANITY', 'ISLAM', 'HINDUISM', 'TAOISM', 'BUDDHISM', 'SHINTO', 'SIKHISM', 'JUDAISM', 'AGNOSTIC', 'ATHEIST');
		$info['educ']					= array('NONE', 'COMPLETED PRIMARY SCHOOL', 'COMPLETED', 'SECONDARY SCHOOL', 'DIPLOMA', 'DEGREE', 'PHD', 'CERTIFICATE OF COMPLETION');
		$info['stat']					= array('SINGLE', 'IN A RELATIONSHIP', 'ENGAGED', 'MARRIED', 'DIVORCED', 'WIDOWED');
		$info['illness']				= array('Mental illness', 'Tuberculosis', 'Epilepsy', 'Heart disease', 'Asthma', 'Malaria', 'Diabetes', 'Operations', 'Hypertension', 'Others');
		$info['food']					= array('No pork', 'No beef', 'Vegetarian', 'Others');
		$info['eval']					= array('Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA', 
													'Interviewed by Singapore EA', 'Interviewed by overseas training centre/EA');
		$info['country']				= $this->Mod_agency->getCountries();
		$info['agencies']				= $this->Mod_profile->get_agencies();

		
		$content['personal_info']			= $this->load->view("admin/profile/personal_info",$info,true);
		$content['medical_history']			= $this->load->view("admin/profile/medical_history",$info,true);
		$content['area_of_work']			= $this->load->view("admin/profile/area_of_work",$info,true);
		$content['media']					= $this->load->view("admin/profile/media_new",$info,true);
		$content['employment_history']		= $this->load->view("admin/profile/employment_history",$info,true);
	

		if($this->input->post()){
			$post = $this->input->post();
			
			$intProfileID = $this->Mod_profile->add($post['personal_info'], "profile_personal_info");
			
			$personal_medical['personal_medical'] = $post['personal_medical'];
			
			$personal_medical['personal_medical']['illness'] 				= isset($post['personal_medical']['illness']) ? implode("|", $post['personal_medical']['illness']) : "";
			$personal_medical['personal_medical']['evaluation_skill'] 	= isset($post['personal_medical']['evaluation_skill']) ?  implode("|", $post['personal_medical']['evaluation_skill']) : "";
			$personal_medical['personal_medical']['profile_id'] 			= $intProfileID;
			$this->Mod_profile->add($personal_medical['personal_medical'], "profile_medical_history");
			
			
			$post['personal_area_of_work']['profile_id'] = $intProfileID;
			$this->Mod_profile->add($post['personal_area_of_work'], "profile_area_of_work");
			#$this->Mod_profile->add(array("profile_id" => $intProfileID), "profile_media");
			
			$arrFiles 	= $this->do_upload($_FILES);
			$arrInsert = array();
			if(isset($arrFiles["picture"])){
				$arrInsert['picture'] = $arrFiles["picture"][0];
			}
			if(isset($arrFiles["video"])){
				$arrInsert['video'] = $arrFiles["video"][0];
			}
			$arrInsert["profile_id"] = $intProfileID;
	
			$this->Mod_profile->add($arrInsert, "profile_media");

			#print_r($post['employment_history']['history']);
				$y = 0;
				for($x = 0;$x <= count($post['employment_history']['history']); $x++){
						if($post['employment_history']['history']['employer'][$x] <> "" AND $post['employment_history']['history']['date_from'][$x] <> "" AND $post['employment_history']['history']['date_to'][$x]
							AND $post['employment_history']['history']['work_duties'][$x] <> "" AND $post['employment_history']['history']['remarks'][$x] <> "" AND $post['employment_history']['history']['emp_feedback'][$x] <> ""){
							$employment_history['employment_history']['employer'] 			= $post['employment_history']['history']['employer'][$x];
							$employment_history['employment_history']['date_from'] 		= $post['employment_history']['history']['date_from'][$x];
							$employment_history['employment_history']['date_to'] 			= $post['employment_history']['history']['date_to'][$x];
							$employment_history['employment_history']['work_duties'] 		= $post['employment_history']['history']['work_duties'][$x];
							$employment_history['employment_history']['remarks'] 			= $post['employment_history']['history']['remarks'][$x];
							$employment_history['employment_history']['emp_feedback'] 	= $post['employment_history']['history']['emp_feedback'][$x];
							$employment_history['employment_history']['profile_id'] 			= $intProfileID;
							$employment_history['employment_history']['country_history'] 	= $post['employment_history']['history']['country_history'][$x];
							$employment_history['employment_history']['prev_work_sg']	= $post['employment_history']['prev_work_sg'];
							$employment_history['employment_history']['availability_fdw']	= isset($post['employment_history']['availability_fdw']) ? json_encode($post['employment_history']['availability_fdw']) : false ;

							$this->Mod_profile->add($employment_history['employment_history'], "profile_employment_history");
						}		
				}
		}
		
		$data['sidemenu'] 	= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 	= $this->load->view("admin/profile_new", $content, true);

		$this->load->view('admin/index', $data);
	}
	
	public function profiles(){
		$var1  						= array();
		$content  					= array();
		$content['pageHeader'] 		= "Profiles";
		$content['id'] 				= $this->userid;
		$var1['id']					= $this->userid;
		
		$content['data']			= $this->Mod_profile->getProfileByAgencyID($this->userid);
		$data['sidemenu'] 			= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 			= $this->load->view("admin/profiles", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function profile_list(){
		
		if($this->session->userdata("userid") == $this->input->get("owner_id")){
		
		$var1  = array();
		$content = array();
		$var1['id']							= $this->userid;
		$content['profile_id']				= $this->input->get("profile_id");
		$content['agency_id']				= $this->input->get("profile_id");
		$content['owner_id']				= $this->input->get("owner_id");
		$content['profile']					= true;
		$content['pageHeader'] = "View Profile";
		$info['rel']					= array('CHRISTIANITY', 'ISLAM', 'HINDUISM', 'TAOISM', 'BUDDHISM', 'SHINTO', 'SIKHISM', 'JUDAISM', 'AGNOSTIC', 'ATHEIST');
		$info['educ']					= array('NONE', 'COMPLETED PRIMARY SCHOOL', 'COMPLETED', 'SECONDARY SCHOOL', 'DIPLOMA', 'DEGREE', 'PHD', 'CERTIFICATE OF COMPLETION');
		$info['stat']					= array('SINGLE', 'IN A RELATIONSHIP', 'ENGAGED', 'MARRIED', 'DIVORCED', 'WIDOWED');
		$info['illness']				= array('Mental illness', 'Tuberculosis', 'Epilepsy', 'Heart disease', 'Asthma', 'Malaria', 'Diabetes', 'Operations', 'Hypertension', 'Others');
		$info['food']					= array('No pork', 'No beef', 'Vegetarian', 'Others');
		$info['eval']					= array('Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA', 
													'Interviewed by Singapore EA', 'Interviewed by overseas training centre/EA');
		
		$info['info']				= $this->Mod_profile->view_profile($this->session->userdata("userid"), $this->input->get("profile_id"));
		$info['country']			= $this->Mod_agency->getCountries();
		$info['agencies']			= $this->Mod_profile->get_agencies();

		
		$content['personal_info']			= $this->load->view("agency/profile/personal_info",$info,true);
		$content['medical_history']			= $this->load->view("agency/profile/medical_history",$info,true);
		$content['area_of_work']			= $this->load->view("agency/profile/area_of_work",$info,true);
		$content['employment_history']		= $this->load->view("agency/profile/employment_history",$info,true);
		$content['media']					= $this->load->view("agency/profile/media",$info,true);
	

		if($this->input->post()){
			$post = $this->input->post();
			if($post['personal_info']['profile_owner'] == 0){
				$post['personal_info']['profile_owner'] = $this->userid;
			}
			$intProfileID = $this->input->get("profile_id");
			$intOwnerID = $this->input->get("owner_id");
			
			$arrWhere = array("id" => $intProfileID);
			$arrWhere1 = array("profile_id" => $intProfileID);

			$this->Mod_profile->update($post['personal_info'], $arrWhere, "profile_personal_info");
			$personal_medical['personal_medical'] = $post['personal_medical'];
			
			$personal_medical['personal_medical']['illness'] 				= isset($post['personal_medical']['illness']) ? implode("|", $post['personal_medical']['illness']) : "";
			$personal_medical['personal_medical']['evaluation_skill'] 		= isset($post['personal_medical']['evaluation_skill']) ?  implode("|", $post['personal_medical']['evaluation_skill']) : "";
			
			$this->Mod_profile->update($personal_medical['personal_medical'], $arrWhere1, "profile_medical_history");

			$post['personal_area_of_work']['profile_id'] = $intProfileID;
			$this->Mod_profile->update($post['personal_area_of_work'], $arrWhere1, "profile_area_of_work");

			for($x = 0;$x <= count($post['employment_history']['history']); $x++){
				if(@$post['employment_history']['history']['employer'][$x] <> "" AND $post['employment_history']['history']['date_from'][$x] <> "" AND $post['employment_history']['history']['date_to'][$x]
					AND $post['employment_history']['history']['work_duties'][$x] <> "" AND $post['employment_history']['history']['remarks'][$x] <> "" AND $post['employment_history']['history']['emp_feedback'][$x] <> ""){
					$employment_history['employment_history']['employer'] 			= $post['employment_history']['history']['employer'][$x];
					$employment_history['employment_history']['date_from'] 		= $post['employment_history']['history']['date_from'][$x];
					$employment_history['employment_history']['date_to'] 			= $post['employment_history']['history']['date_to'][$x];
					$employment_history['employment_history']['work_duties'] 		= $post['employment_history']['history']['work_duties'][$x];
					$employment_history['employment_history']['remarks'] 			= $post['employment_history']['history']['remarks'][$x];
					$employment_history['employment_history']['emp_feedback'] 	= $post['employment_history']['history']['emp_feedback'][$x];
					$employment_history['employment_history']['country_history'] 	= $post['employment_history']['history']['country_history'][$x];
					$employment_history['employment_history']['profile_id'] 			= $intProfileID;
					$employment_history['employment_history']['prev_work_sg']	= $post['employment_history']['prev_work_sg'];
					$employment_history['employment_history']['availability_fdw']	= isset($post['employment_history']['availability_fdw']) ? json_encode($post['employment_history']['availability_fdw']) : false ;
					$arrWhere2 = array("id" => $post['employment_history']['history']['emp_id'][$x], "profile_id" => $intProfileID);
					$this->Mod_profile->update($employment_history['employment_history'], $arrWhere2, "profile_employment_history");
				}		
			}
				
			if($post['personal_info']['profile_owner'] <> $this->userid){
				$content['submit'] 		= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Profile Successfully Transferred";
				$this->session->set_flashdata('flsh_msg', $content);
				redirect("admin/profile/profiles");
			}else{
				$content['submit'] 		= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Profile has been Updated";
				$this->session->set_flashdata('flsh_msg', $content);
				redirect("admin/profile/profile_list?owner_id={$intOwnerID}&profile_id={$intProfileID}");
			}
		}
		
		$data['sidemenu'] 	= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 	= $this->load->view("admin/profile_view", $content, true);

		$this->load->view('admin/index', $data);	
		}else{
			show_404(); 
		}
	}
	
}

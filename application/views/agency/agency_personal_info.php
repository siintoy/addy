<div class="form-group">
	<label for="name">Name:</label>
	<input required type="text"  placeholder="Name"  class="form-control" id="name" name="personal_info[name]" />
	<?php if($this->session->userdata("role") == 1): ?>
		<input required type="hidden" class="form-control" value="<?php echo $this->session->userdata("userid"); ?>" id="owner_id" name="personal_info[profile_owner]" />
	<?php endif; ?>
</div>

<div class="form-group">
	<label for="datepicker">Date of Birth:</label>
	<input required type="text"  placeholder="Date of Birth"  class="form-control datepicker" id="datepicker" name="personal_info[date_of_birth]" />
</div>

<div class="form-group">
	<label for="place_of_birth">Place of Birth:</label>
	<input required type="text"  placeholder="Place of Birth"  class="form-control" id="place_of_birth" name="personal_info[place_of_birth]" />
</div>

<div class="form-group">
	<label for="country">Country:</label>
	<select required name="personal_info[country]" id="country" class="form-control">
		<?php foreach($country as $countries): ?>
		<option value="<?php echo $countries->code; ?>"><?php echo $countries->name; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="age">Age:</label>
	<select  class="form-control" required name="personal_info[age]" id="age">
			<option  value="">Please Select Age</option>
		<?php for($x = 17;$x <= 120;$x++): ?>
			<option  value="<?php echo $x; ?>"><?php echo $x; ?></option>
		<?php endfor; ?>
	</select>
</div>

<div class="form-group">
	<label for="height">Height:</label>
	<input required type="text" onkeypress='return isNumberKey(event);' placeholder="Height"  class="form-control" id="height" name="personal_info[height]" />
</div>

<div class="form-group">
	<label for="weight">Weight:</label>
	<input required type="text" onkeypress='return isNumberKey(event);' placeholder="Weight"  class="form-control" id="weight" name="personal_info[weight]" />
</div>

<div class="form-group">
	<label for="nationality">Nationality:</label>
	<input required type="text"  placeholder="Nationality"  class="form-control" id="nationality" name="personal_info[nationality]" />
</div>

<div class="form-group">
	<label for="home_address">Residential address in home country:</label>
	<textarea required type="text"  placeholder="Residential address in home country"  class="form-control" id="home_address" name="personal_info[home_address]" ></textarea>
</div>

<div class="form-group">
	<label for="airport">Name of port / airport to be repatriated to:</label>
	<input required type="text"  placeholder="Name of port / airport to be repatriated to"  class="form-control" id="airport" name="personal_info[airport]" />
</div>

<div class="form-group">
	<label for="home_number">Contact number in home country:</label>
	<input required type="text"  placeholder="Contact number in home country"  class="form-control" id="home_number" name="personal_info[home_number]" />
</div>

<div class="form-group">
	<label for="religion">Religion:</label>
	<select  class="form-control" required name="personal_info[religion]" id="religion">
			<option  value="">Please Select Religion</option>
		<?php foreach($rel as $e): ?>
			<option  value="<?php echo $e; ?>"><?php echo $e; ?></option>
		<?php endforeach; ?>
	</select>
</div>
 
<div class="form-group">
	<label for="education">Education level:</label>
	<select  class="form-control" required name="personal_info[education]" id="education">
			<option  value="">Please Select Education</option>
		<?php foreach($educ as $w): ?>
			<option  value="<?php echo $w; ?>"><?php echo $w; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="no_of_siblings">Number of siblings:</label>
	<input required type="text" onkeypress='return isNumberKey(event);' placeholder="Number of siblings"  class="form-control" id="no_of_siblings" name="personal_info[no_of_siblings]" />
</div>

<div class="form-group">
	<label for="marital_status">Marital status:</label>
	<select  class="form-control" required name="personal_info[marital_status]" id="marital_status">
			<option  value="">Please Select Education</option>
		<?php foreach($stat as $q): ?>
			<option  value="<?php echo $q; ?>"><?php echo $q; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="no_of_children">Number of Children:</label>
	<input required type="text"  placeholder="Number of Children" onkeypress='return isNumberKey(event);' class="form-control" id="no_of_children" name="personal_info[no_of_children]" />
</div>

<div class="form-group">
	<label for="age_of_childing">Age(s) of children (if any):</label>
	<input  type="text"   placeholder="Age(s) of children (if any)"  class="form-control" id="age_of_childing" name="personal_info[age_of_childing]" />
</div>
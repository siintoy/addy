<style>
	.isHidden{
		display:none;
	}
	.isShown{
		display:inline;
	}
</style>
<?php 
	$title = "(The EA is required to obtain the FDW&#39;s employment history from MOM and furnish the employer with the employment history of the FDW. The employer may also verify the FDW&#39;s employment history in Singapore through WPOL using SingPass)"; 
	$arrAvailability = array("FDW is not available for interview", "FDW can be interviewed by phone", "FDW can be interviewed by video-conference", "FDW can be interviewed in person");
?>
<div class="form-group">
	<label>Employment History Overseas</label>
</div>
<div id="form_employment_history"  class="form-group">
	<div class="emp" id="employment_history" class="form-group">
		<label for="employer">Employer:</label>
		<div style="float:right;">
			<a id="show" href="#" class="btn btn-default btn-sm">
				<span class="glyphicon glyphicon-plus"></span>
			</a> 
			<a id="hidden" href="#" class="btn btn-default btn-sm">
				<span class="glyphicon glyphicon-minus"></span>
			</a>
		</div>
<?php for($x = 0; $x <= 10; $x++): ?>
<div id="tbl_tform_employment_history<?php echo $x;?>"  class="emp_form <?php echo ($x > 0 ? "isHidden": "isShown" ); ?>">
		<table class="table table-nonfluid">
			<tr>
				<td  colspan="2">
					<input   type="text"  placeholder="Employer"  class="form-control employer empcount0" id="employer"  name="employment_history[history][employer][]" />
				</td>
			</tr>
			<tr>
				<td>
					<input  type="text"  placeholder="From"  class="form-control datepicker" name="employment_history[history][date_from][]" />
				</td>
				<td>
					<input  type="text"  placeholder="To"  class="form-control datepicker" name="employment_history[history][date_to][]" />
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<select  name="employment_history[history][country_history][]" id="country" class="form-control">
						<option value="0">Select Country</option>
						<?php foreach($country as $countries): ?>
						<option value="<?php echo $countries->code; ?>"><?php echo $countries->name; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<textarea  placeholder="Work Duties"  class="form-control" id="work_duties" name="employment_history[history][work_duties][]" ></textarea>
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<textarea  placeholder="Remarks"  class="form-control" id="remarks" name="employment_history[history][remarks][]" ></textarea>
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<textarea  placeholder="Employer Feedback"  class="form-control" id="emp_feedback" name="employment_history[history][emp_feedback][]" ></textarea>
				</td>
			</tr>
		</table>
	</div>
<?php endfor; ?>
	
	<div  class="form-group">
		<label for="">Employment History in Singapore</label>
	</div>
	
	<div  class="form-group">
		<label for="prev_work_sg">Previous working experience in Singapore:</label>
		<select title="<?php echo $title; ?>" required class="form-control" id="prev_work_sg" name="employment_history[prev_work_sg]" >
			<option value="1">Yes</option>
			<option value="0">No</option>
		</select>
	</div>
	
	<div  class="form-group">
		<label for="availability_fdw">Availability of FDW to be interviewed by prospective employer:</label> <br />
		<?php $x = 0; foreach($arrAvailability as $z): ?>
			<label><input name="employment_history[availability_fdw][]" type="checkbox" value="<?php echo $z; ?>"> <?php echo $z; ?> </label><br />
		<?php $x++; endforeach; ?>
	</div>

	</div>
</div>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
	/*
	var form_employment_history = 0;
	$("#clone").click(function(){
		form_employment_history = form_employment_history + 1;
		$( "#employment_history").clone().appendTo( "#form_employment_history" ).addClass("emp_history new_emp" + form_employment_history);
		
		$(".new_emp" + form_employment_history + " input").val("");
		// $(".emp_history").each(function(){
			//form_employment_history = form_employment_history + 1;
			// $(this).val("");
		// });
	});
	
	$("#remove").click(function(){
		var x = "";
		$(".emp_history").each(function(){
			x = $(this).attr("class").split(" ")[2];
		});
		console.log(x);
		
		$("."+x).remove();
	});
	*/
			var q = 0; 

	$("#show").click(function(){
		if(q >= 0){
			q = q + 1;
			console.log("#tbl_tform_employment_history"+q);
			// $(".emp_form").each(function(x, y){

		 // if($(this).attr("class").split(" ")[1] == "isShown"){
		   // q = q +1;
		   // console.log("#tbl_tform_employment_history"+q);
		  // $("#tbl_tform_employment_history"+q).removeClass("isHidden");
		 // $("#tbl_tform_employment_history"+q).addClass("isShown");
		 // }		
		//});
		 $("#tbl_tform_employment_history"+q).removeClass("isHidden");
		 $("#tbl_tform_employment_history"+q).addClass("isShown");
		}
	});
	
	$("#hidden").click(function(){
	  if(q > 0){  
	    $("#tbl_tform_employment_history"+q).find('input:text').val('');
	    $("#tbl_tform_employment_history"+q).find('textarea').val('');
	    $("#tbl_tform_employment_history"+q).find('select').val('');
		
		console.log("#tbl_tform_employment_history"+q);
		$("#tbl_tform_employment_history"+q).removeClass("isShown");
		$("#tbl_tform_employment_history"+q).addClass("isHidden");
		q = q - 1;
	  }
	});
	
	
});
</script>
<style>
	.isHidden{
		display:none;
	}
	.isShown{
		display:inline;
	}
</style>
<?php 
	$title = "(The EA is required to obtain the FDW&#39;s employment history from MOM and furnish the employer with the employment history of the FDW. The employer may also verify the FDW&#39;s employment history in Singapore through WPOL using SingPass)"; 
	$arrAvailability = array("FDW is not available for interview", "FDW can be interviewed by phone", "FDW can be interviewed by video-conference", "FDW can be interviewed in person");
	
	$arrAvail = isset($info[0]->availability_fdw) ? json_decode($info[0]->availability_fdw, true) : null;
	
	foreach($arrAvailability as $oo){
		if(!@in_array($oo, $arrAvail)){
			$newArr[] = $oo;
		}
	}
	?>
<div class="form-group">
	<label>Employment History Overseas</label>
</div>
<div id="form_employment_history"  class="form-group">
	<div class="emp" id="employment_history" class="form-group">
	<label for="employer">Employer:</label>
<?php foreach($info as $emp): ?>
<input value="<?php echo isset($emp->emp_id) ? $emp->emp_id : false ;?>" type="hidden"  placeholder="Employer"  class="form-control employer empcount0" id="emp_id"  name="employment_history[history][emp_id][]" />
<div id="tbl_tform_employment_history"  class="emp_form">
		<table class="table table-nonfluid">
			<tr>
				<td  colspan="2">
					<input value="<?php echo isset($emp->employer) ? $emp->employer : false ;?>" type="text"  placeholder="Employer"  class="form-control employer empcount0" id="employer"  name="employment_history[history][employer][]" />
				</td>
			</tr>
			<tr>
				<td>
					<input value="<?php echo isset($emp->date_from) ? $emp->date_from : false ;?>" type="text"  placeholder="From"  class="form-control datepicker" name="employment_history[history][date_from][]" />
				</td>
				<td>
					<input value="<?php echo isset($emp->date_to) ? $emp->date_to : false ;?>" type="text"  placeholder="To"  class="form-control datepicker" name="employment_history[history][date_to][]" />
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<select  name="employment_history[history][country_history][]" id="country" class="form-control">
						<option value="0">Select Country</option>
						<?php foreach($country as $countries): ?>
						<option <?php echo $emp->country_history == $countries->code ? " selected " : false ;?> value="<?php echo $countries->code; ?>"><?php echo $countries->name; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<textarea placeholder="Work Duties"  class="form-control" id="work_duties" name="employment_history[history][work_duties][]" ><?php echo isset($emp->work_duties) ? $emp->work_duties : false ;?></textarea>
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<textarea  placeholder="Remarks"  class="form-control" id="remarks" name="employment_history[history][remarks][]" ><?php echo isset($emp->remarks) ? $emp->remarks : false ;?></textarea>
				</td>
			</tr>
			<tr>
				<td  colspan="2">
					<textarea  placeholder="Employer Feedback"  class="form-control" id="emp_feedback" name="employment_history[history][emp_feedback][]" ><?php echo isset($emp->emp_feedback) ? $emp->emp_feedback : false ;?></textarea>
				</td>
			</tr>
		</table>
	</div>
<?php endforeach; ?>
	
	<div  class="form-group">
		<label for="">Employment History in Singapore</label>
	</div>
	
	<div  class="form-group">
		<label for="prev_work_sg">Previous working experience in Singapore:</label>
		<select title="<?php echo $title; ?>" required class="form-control" id="prev_work_sg" name="employment_history[prev_work_sg]" >
			<option <?php echo $info[0]->prev_work_sg == 1 ? " selected " : false ;?> value="1">Yes</option>
			<option <?php echo $info[0]->prev_work_sg == 0 ? " selected " : false ;?> value="0">No</option>
		</select>
	</div>
	
	<div  class="form-group">
		<label for="availability_fdw">Availability of FDW to be interviewed by prospective employer:</label> <br />
		<?php if(count($arrAvail) > 0):  foreach($arrAvail as $z): ?>
			<label><input name="employment_history[availability_fdw][]" checked type="checkbox" value="<?php echo $z; ?>"> <?php echo $z; ?> </label><br />
		<?php  endforeach; endif; ?>
		<?php $x = 0; foreach($newArr as $z): ?>
			<label><input name="employment_history[availability_fdw][]" type="checkbox" value="<?php echo $z; ?>"> <?php echo $z; ?> </label><br />
		<?php $x++; endforeach; ?>
	</div>
	</div>
</div>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
	var q = 0; 
	$("#show").click(function(){
		if(q >= 0){
			q = q + 1;
			console.log("#tbl_tform_employment_history"+q);
			// $(".emp_form").each(function(x, y){

		 // if($(this).attr("class").split(" ")[1] == "isShown"){
		   // q = q +1;
		   // console.log("#tbl_tform_employment_history"+q);
		  // $("#tbl_tform_employment_history"+q).removeClass("isHidden");
		 // $("#tbl_tform_employment_history"+q).addClass("isShown");
		 // }		
		//});
		 $("#tbl_tform_employment_history"+q).removeClass("isHidden");
		 $("#tbl_tform_employment_history"+q).addClass("isShown");
		}
	});
	
	$("#hidden").click(function(){
	  if(q > 0){  
	    $("#tbl_tform_employment_history"+q).find('input:text').val('');
	    $("#tbl_tform_employment_history"+q).find('textarea').val('');
	    $("#tbl_tform_employment_history"+q).find('select').val('');
		
		console.log("#tbl_tform_employment_history"+q);
		$("#tbl_tform_employment_history"+q).removeClass("isShown");
		$("#tbl_tform_employment_history"+q).addClass("isHidden");
		q = q - 1;
	  }
	});
	
	
});
</script>
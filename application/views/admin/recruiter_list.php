<div style="padding:10px;" class="row">
	<div class="">
		<div class="dataTable_wrapper">
	<?php if(!empty($data)): ?>
		<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
		  <tr>
			<th>Username</th>
			<th>Recruiter Name</th>
			<th>Recruiter Phone</th>
			<th>Country</th>
			<!-- <th>Status</th> -->
			<th>Action</th>
		  </tr>
		</thead>
		<tbody>
		<?php foreach($data->result() as $rows): ?>
		  <tr>
			<td><?php echo $rows->username; ?></td>
			<td><?php echo $rows->name; ?></td>
			<td><?php echo $rows->phone; ?></td>
			<td><?php echo $rows->cname; ?></td>
			<?php /* <td>
				<center>
				 <div class="dropdown">
				  <button class="btn-sm btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo ucwords($rows->status); ?>
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
					<?php foreach($status as $s): ?>
						<li class="<?php echo ($s == $rows->status ? "disabled" : false); ?>"><a  href="<?php echo site_url("admin/recruiter/status?action=".$s."&id=".$rows->userid."");?>"><?php echo ucwords($s); ?></a></li>
					<?php endforeach; ?>
				  </ul>
				</div>
				</center>
			</td> */ ?>
			<td>
				<center>
					<a href="<?php echo site_url('admin/recruiter/view?id='.$rows->userid.'&username='.$rows->username.''); ?>">View</a>  | 
					<a data-toggle="modal" data-target="#myModal" onclick="return deleteUser('<?php echo $rows->username; ?>', <?php echo $rows->userid; ?>);" href="#">Delete</a>
				</center>
			</td>

		  </tr>
		<?php endforeach; ?>
		</tbody>
	  </table>
	 <?php endif; ?>
	</div>
</div>	

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure want to delete <span id="delUsername"></span> ?.</p>
        </div>
        <div class="modal-footer">
		<form action="<?php echo site_url("admin/recruiter/delete");?>" method="post">
			<button type="submit" name="delete" id="delete" value="" class="btn btn-primary">Delete</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</form>
        </div>
      </div>
      
    </div>
  </div>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <script>
	$(document).ready(function() {
		$("#delete").click(function() {
			val = $(this).val();
		});
	});
	function deleteUser(username, userid){
		$("#delUsername").html(username);
		$("#delete").val(userid);
	}
 </script>
  
<ul class="nav navbar-nav side-nav">
	<li><a href="<?php echo site_url("admin/agency?id={$id}");?>"><i class="glyphicon glyphicon-bullhorn"></i> FAQs</a></li>
	<li><a href="<?php echo site_url("admin/agency/about_us?id={$id}");?>"><i class="glyphicon glyphicon-bullhorn"></i> About Us</a></li>
	<li><a href="<?php echo site_url("admin/agency/settings?id={$id}");?>"><i class="glyphicon glyphicon-bullhorn"></i> Settings</a></li>
	<li><a href="<?php echo site_url("admin/agency/news?id={$id}");?>"><i class="glyphicon glyphicon-bullhorn"></i> Latest News</a></li>
	<li><a href="<?php echo site_url("admin/agency/profiles?id={$id}");?>"><i class="glyphicon glyphicon-bullhorn"></i> Profiles</a></li>
</ul>
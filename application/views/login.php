<div class="row">
	<div class="col-md-4 col-md-offset-4" >
		<div class="login-panel panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><center><b>ProfileXChange</b></center></h3>
			</div>
			<div class="panel-body">
				<!-- <div><center><img style="height:76px;width:190px;" src="../uploads/hiro_color.gif"/></center></div> -->
				<?php if(isset($msg)): ?><center><div style="color:red"><?php echo $msg; ?></div></center><br><?php endif; ?>
				<form method="post" role="form">
					<fieldset>
						<div class="form-group">
							<input class="form-control" required placeholder="Username" name="username" type="username" autofocus value="<?php echo (isset($username) ? $username : false ); ?>"/>
						</div>
						<div class="form-group">
							<input class="form-control" required placeholder="Password" name="password" type="password" value="<?php echo (isset($password) ? $password : false ); ?>" />
						</div>
						<div class="form-group">
							<a href="#"><center>Reset Password</center></a>
						</div>
						<!-- 
						<div class="checkbox">
							<label>
								<input name="remember" type="checkbox" value="Remember Me">Remember Me
							</label>
						</div> -- >
						<!-- Change this to a button or input when using this as a form -->
						<center><input type="submit" style="width:100px;" class="btn-sm btn btn-lg btn-success btn-block" value="Login" /></center>
					</fieldset>
				</form>
			</div>
			<div>
				<center><small>Please contact us at 8399030 if you encounter any problems.</small>	</center>
			</div>
		</div>
		
	</div>
</div>
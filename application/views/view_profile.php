<style>
.img{
	padding:5px;
}

.profile_desc{
    overflow: hidden;
    width: 97%;
	border:1px solid #ccc;
	padding:10px;
}

.bold{
	font-weight:bolder;

}

</style>

<div class="col-xs-13">
	<div class="container-fluid">
		<?php $x = 0; foreach($result as $rows): ?>
			<?php if($x == 0): ?>
			<div style="border:1px solid #ccc"  class="panel panel-default">
				<div style="border-bottom:1px solid #ccc"  class="panel-heading">
					<h4>Profile Information</h4>
				</div >
				<div class="img">
					<img class="img-thumbnail" style="height:200px;width:200px" src="<?php echo base_url(); ?>uploads/<?php echo $rows->picture; ?>"/>
					<div style="padding:5px;"><br>
					 <button data-toggle="modal" data-target="#myModalVideo" type="button" class="btn btn-primary btn-sm <?php echo ($rows->video ? $rows->video : "disabled" ); ?>">Watch Video</button>
					 <button data-toggle="modal" data-target="#myModalContactUs" type="button" class="btn btn-info btn-sm">Contact Agency</button><br>
					 <b>Chargeable Fee:</b> <b><i>$ <?php echo number_format($rows->fee, 2); ?> (SGD)</i></b>
					</div>
				</div>
				<div class="panel-body">
				
					<table class="table table-nonfluid table-bordered">
						<tr>
							<td class="col-md-2 bold" align="right">Name: </td>
							<td><?php echo $rows->profname; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Nationality: </td>
							<td><?php echo $rows->nationality; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Date of Birth: </td>
							<td><?php echo $rows->date_of_birth; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Age: </td>
							<td><?php echo $rows->age; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Place of Birth: </td>
							<td><?php echo $rows->place_of_birth; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Height: </td>
							<td><?php echo $rows->height; ?> (cm)</td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Weight: </td>
							<td><?php echo $rows->weight; ?> (kg)</td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Country: </td>
							<td><?php echo ucwords(strtolower($rows->country_name)); ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Religion: </td>
							<td><?php echo ucwords(strtolower($rows->religion)); ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Marital Status: </td>
							<td><?php echo ucwords(strtolower($rows->marital_status)); ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Number of Children: </td>
							<td><?php echo ucwords(strtolower($rows->no_of_children)); ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Home Address: </td>
							<td><?php echo $rows->home_address; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Repatriating Port: </td>
							<td><?php echo $rows->airport; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Education: </td>
							<td><?php echo ucwords(strtolower($rows->education)); ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Language: </td>
							<td><?php echo ucwords(strtolower($rows->language)); ?></td>
						</tr>
					</table>
				</div>
			</div>
			<?php 
					$strInquire = "ID: {$rows->id}|Name: {$rows->profname}";
					$arrCare = array(
								"care_of_infants_0to2" 		=> "Care of Infants 0 to 2",
								"care_of_infants_3to5" 		=> "Care of Infants 3 to 5",
								"care_of_infants_6to8" 		=> "Care of Infants 6 to 8",
								"care_of_infants_9to11" 	=> "Care of Infants 9 to 11",
								"care_of_infants_12to14" 	=> "Care of Infants 12 to 14",
								"care_for_elderly" 			=> "Care for Elderly",
								"care_for_disabled" 		=> "Care for Disabled",
								"general_housework" 		=> "General Housework",
								"cooking" 					=> "Cooking"
								); 
								
					$arrRate = array(
								"care_of_infants_0to2" 		=> $rows->rate_care_of_infants_0to2,
								"care_of_infants_3to5" 		=> $rows->rate_care_of_infants_3to5,
								"care_of_infants_6to8" 		=> $rows->rate_care_of_infants_6to8,
								"care_of_infants_9to11" 	=> $rows->rate_care_of_infants_9to11,
								"care_of_infants_12to14" 	=> $rows->rate_care_of_infants_12to14,
								"care_for_elderly" 			=> $rows->rate_care_for_elderly,
								"care_for_disabled" 		=> $rows->rate_care_for_disabled,
								"general_housework" 		=> $rows->rate_general_housework,
								"cooking" 					=> $rows->rate_cooking
								); 
			?>
			
			<div style="border:1px solid #ccc"  class="panel panel-default">
				<div style="border-bottom:1px solid #ccc"  class="panel-heading">
					<h4>Area of Work</h4>
				</div >
				<div class="panel-body">
					<table class="table table-nonfluid table-bordered">
						<tr>
							<td class="col-md-2 bold" align="right">Areas of Work</td>
							<td align="center">Willingness</td>
							<td>Evaluation</td>
						</tr>
						<?php foreach($arrCare as $key=>$val): ?>
						<tr>
							<td class="col-md-2 bold" align="right"><?php echo $val; ?>: </td>
							<td align="center"><?php echo ($rows->$key == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>"); ?></td>
							<td>
								<?php for($t = 1;$t <= $arrRate[$key];$t++): ?>
									<span class='glyphicon glyphicon-star'></span>
								<?php endfor; ?>
								<?php for($u = 5;$u > $arrRate[$key];$u--): ?>
									<span class='glyphicon glyphicon-star-empty'></span>
								<?php endfor; ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<tr>
							<td class="col-md-2 bold" align="right">Other Skills</td>
							<td colspan="2"><?php echo $rows->other_skills; ?></td>
						</tr>
					</table>
				</div>
			</div>
			<div style="border:1px solid #ccc"  class="panel panel-default">
				<div style="border-bottom:1px solid #ccc"  class="panel-heading">
					<h4>Other Information</h4>
				</div >
				<div class="panel-body">
					<table class="table table-nonfluid table-bordered ">
						<tr>
							<td class="col-md-2 bold" align="right">Allergies: </td>
							<td><?php echo $rows->allergies; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Illness: </td>
							<td><?php echo str_replace("|", "<br />", $rows->illness); ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Disabilities: </td>
							<td><?php echo  $rows->disabilities; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Food Preferences: </td>
							<td><?php echo  $rows->food_preferences; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Other Remarks: </td>
							<td><?php echo  $rows->others; ?></td>
						</tr>
						<tr>
							<td class="col-md-2 bold" align="right">Evaluation Skill: </td>
							<td><?php echo str_replace("|", "<br />", $rows->evaluation_skill); ?></td>
						</tr>
					</table>
				</div>
			</div>
			  <div class="modal fade" id="myModalVideo" role="dialog">
				<div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div align="center" class="embed-responsive embed-responsive-16by9">
								<video controls class="embed-responsive-item">
									<source src="<?php echo base_url();?>uploads/<?php echo $rows->video; ?>" type="video/mp4">
								</video>
							</div>
						</div>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="modal fade" id="myModalContactUs" role="dialog">
					<div class="modal-dialog">
					
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">CONTACT AGENCY</h4>
						</div>
						<div class="modal-body">
							<div class="">
								 <form role="form" method="post" action="<?php echo base_url();?>index.php/main/contact_us?id=<?php echo $id; ?>">
									  <div class="form-group">
										<label for="name">Your Name:</label>
										<input required type="text"  placeholder="Your Name"  class="form-control" id="name" name="name">
									  </div>
									  
									  <div class="form-group">
										<label for="email">Your Email:</label>
										<input required type="email"  placeholder="Your Email"  class="form-control" id="email" name="email">
									  </div>
									  
									  <div class="form-group">
										<label for="phone">Your Phone:</label>
										<input required type="text"  placeholder="Your Phone"  class="form-control" id="phone" name="phone">
									  </div>
									  
									  <div class="form-group">
										<label for="message">Message:</label>
										<textarea required class="form-control" id="message" placeholder="Message" name="message"></textarea>
									  </div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-primary"  value="Submit" />
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					 </form>
					  </div>
					  
					</div>
				  </div>
			<?php endif; ?>
		<?php $x++; endforeach; ?>
</div>
<br /> <br/>
</div>


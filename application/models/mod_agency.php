<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_agency extends CI_Model {

	public function add($data = array(), $table = "agency"){
		$this->db->insert($table, $data);
	}
	
	public function get_data($data = array()){
		
		$sql = "SELECT 
					  a.*,
					  a.userid,
					  b.username,
					  c.`name` cname
					FROM
					  `agency` a 
					  JOIN `user` b 
						ON b.`id` = a.`userid` 
					  JOIN country c 
						ON c.`code` = a.`country` 
					WHERE  b.status = 1 
					ORDER BY b.username ";
		
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	public function delete($id = 0){
		$this->db->where('id', $id);
		$data = array('status' => 0);
		$this->db->update('user', $data);
	}
	
	public function getCountries(){
		$sql = "SELECT 
					  * 
					FROM
					  `country` a 
					ORDER BY a.`name` ASC ";
					
		return $this->db->query($sql)->result();
	}
	
	public function updateStatus($status = "", $userid = 0){
		$this->db->where("userid", $userid);
		$this->db->update("agency", array("status" => $status));
	}
	
	public function get_agency($id = 0){
			$sql = "SELECT 
					  * 
					FROM
					  `agency` a 
					WHERE userid = {$id} ";
					
		return $this->db->query($sql)->result();
	}
	
	public function update($data = array(), $userid = 0){
		$this->db->where('userid', $userid);
		$this->db->update('agency', $data);
	}
	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class recruiter extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
		 
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('isLoggedin')){
			redirect("login");
		}elseif($this->session->userdata("role") == 2){
			redirect("admin");
		}
		
		if($this->input->get("id") <> $this->session->userdata("userid") AND $this->session->userdata("role") <> 0 )show_404();
	}
	 
	public function index($data = array())
	{
		$var1 						 	= array();
		$content  						= array();
		$content['pageHeader'] 			= "Dashboard";
		$data['sidemenu'] 				= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 				= $this->load->view("recruiter/index", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function new_recruiter($data = array())
	{
		$var1  		= array();
		$content  	= array();
		$isExist 		= false;
		$intUserID 	= 0;
		
		if($this->input->post()){
			foreach($this->input->post() as $key=>$val):
				$arrInput[$key] = $val;
			endforeach;
				$isExist = $this->Mod_user->hasData($arrInput['user']['username']);
			if(!$isExist){
				$arrUserCredentials['username'] 	= $arrInput['user']['username'];
				$arrUserCredentials['password'] 	= md5($arrInput['user']['password']);
				$arrUserCredentials['user_type'] 	= 1; // recruiter
				$intUserID = $this->Mod_user->add($arrUserCredentials);
				
				$arrInput['credentials']['userid'] 	= $intUserID;
				
				$content['submit'] 	= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Recruiter has been added";
				
				$this->Mod_recruiter->add($arrInput['credentials']);
			}else{
				$content['submit'] 	= true;
				$content['class'] 		= "alert alert-danger";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Error";
				$content['msg'] 		= "Recruiter already exist";
			}
		}
		
		$content['country']			= $this->Mod_recruiter->getCountries();
		$content['pageHeader'] 	= "New Recruiter";
		$data['sidemenu'] 			= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 				= $this->load->view("admin/recruiter_new", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function recruiter_list($data = array())
	{
		$var1  						= array();
		$content  					= array();
		
		$content['data'] 			= $this->Mod_recruiter->get_data();
		$content['status'] 		= array("active","inactive","suspended");
		$content['pageHeader'] = "Recruiter List";
		$data['sidemenu'] 		= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 			= $this->load->view("admin/recruiter_list", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function delete($id = 0){
		$id = $this->input->post("delete");
		$this->Mod_recruiter->delete($id);
		redirect("admin/recruiter/recruiter_list");
	}
	
	public function status($status = ""){
		$status 	= $this->input->get("action");
		$userid 	= $this->input->get("id");
		$this->Mod_recruiter->updateStatus($status, $userid);
		
		redirect("admin/recruiter/recruiter_list");
	}
	
	public function view($userid = 0){
		$var1  							= array();
		$content  						= array();
		$content['country']				= $this->Mod_recruiter->getCountries();
		$content['pageHeader'] 			= "View " . $this->input->get("username");
		$content['data'] 				= $this->Mod_recruiter->get_recruiter($this->input->get("id"));
		if($this->input->post()){
			foreach($this->input->post() as $key=>$val):
				$arrInput[$key] 	= $val;
			endforeach;

				$content['submit'] 	= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Recruiter has been Updated";
				$this->session->set_flashdata('flsh_msg', $content);
				$this->Mod_recruiter->update($arrInput['credentials'], $this->input->get("id"));
				redirect("admin/recruiter/view?id=".$this->input->get('id')."&username=".$this->input->get("username")."");
		}
		$data['sidemenu'] 			= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 				= $this->load->view("admin/recruiter_view", $content, true);
		$this->load->view('admin/index', $data);
	}

}

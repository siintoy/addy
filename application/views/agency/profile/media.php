<div class="form-group">
 <img src="<?php echo base_url();?>uploads/<?php echo $info[0]->picture; ?>" class="img-thumbnail img-responsive" alt="" width="200" height="200">
</div>
<div class="form-group">
	<div align="center" class="embed-responsive embed-responsive-16by9">
		<video controls class="embed-responsive-item">
			<source src="<?php echo base_url();?>uploads/<?php echo $info[0]->video; ?>" type="video/mp4">
		</video>
	</div>
</div>
<br>
<div class="form-group">
	<input type="submit" class="btn btn-primary"  value="Update Profile" />
</div>
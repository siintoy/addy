<?php $disabled = $this->session->userdata("role") > 0 ? " disabled " : false; ?>
<div style="padding:10px;" class="row">
<?php if($flash = $this->session->flashdata('flsh_msg')): ?>
  <div class="<?php echo $flash['class']; ?>">
    <a href="#" class="close" data-dismiss="<?php echo $flash['dismiss']; ?>" aria-label="close">&times;</a>
    <strong><?php echo $flash['errType']; ?>!: </strong> <?php echo $flash['msg']; ?>.
  </div> 
<?php endif; ?>
  <!-- Modal content-->
 <?php foreach($data as $rows): ?>
    <div class="">
      <div class="">
         <form role="form" method="post" action="<?php echo base_url();?>index.php/admin/agency/view?id=<?php echo $this->input->get("id"); ?>&username=<?php  echo $this->input->get("username"); ?>">
		  <div class="form-group">
			<label for="name">Agency Name:</label>
			<input required type="text" value="<?php echo $rows->name; ?>" placeholder="Agency Name"  class="form-control" id="name" name="credentials[name]">
		  </div>
		  <div class="form-group">
			<label for="address">Agency Address:</label>
			<input required type="text" value="<?php echo $rows->address; ?>"  placeholder="Agency Address"  class="form-control" id="address" name="credentials[address]"/>
		  </div>
		  <div class="form-group">
			<label for="phone">Agency Phone:</label>
			<input required type="text" value="<?php echo $rows->phone; ?>"   placeholder="Agency Phone"  class="form-control" id="phone" name="credentials[phone]">
		  </div>
		  <div class="form-group">
			<label for="country">Country:</label>
			<select required name="credentials[country]" id="country" class="form-control">
				<?php foreach($country as $countries): ?>
				<option <?php echo ($rows->country == $countries->code ? " selected " : false); ?> value="<?php echo $countries->code; ?>"><?php echo $countries->name; ?></option>
				<?php endforeach; ?>
			</select>
			</div>
		  <div class="form-group">
			<label for="contact_person">Contact Person:</label>
			<input required type="text" value="<?php echo $rows->contact_person; ?>"  class="form-control" placeholder="Contact Person"   id="contact_person" name="credentials[contact_person]">
		  </div>
		  <div class="form-group">
			<label for="contact_person_no">Contact Person No.:</label>
			<input required type="text" value="<?php echo $rows->contact_person_no; ?>" class="form-control" placeholder="Contact Person No."  id="contact_person_no" name="credentials[contact_person_no]">
		  </div>
		  <div class="form-group">
			<label for="contact_person_email">Contact Person Email:</label>
			<input required type="email" value="<?php echo $rows->contact_person_email; ?>" class="form-control" placeholder="Contact Person Email" id="contact_person_email" name="credentials[contact_person_email]">
		  </div>
		  <div class="form-group">
			<label for="interview">Available Form of Interview: (Phone / Skype)</label>
			<input <?php echo $disabled; ?> required type="text" value="<?php echo $rows->interview; ?>" placeholder=" Interview Phone Number / Skyper Username " class="form-control" id="interview" name="credentials[interview]"/>
			</div>
		  <div class="form-group">
			<label for="bus_reg_no">Agency Business Registration No.:</label>
			<input <?php echo $disabled; ?> required type="text" value="<?php echo $rows->bus_reg_no; ?>" placeholder="Agency Business Registration No." class="form-control" id="bus_reg_no" name="credentials[bus_reg_no]">
			</div>
		  <div class="form-group">
			<label for="url">Agency URL:</label>
			<input <?php echo $disabled; ?> required type="text" value="<?php echo $rows->url; ?>" placeholder="Agency URL" class="form-control" id="url" name="credentials[url]">
			</div>
		  <div class="form-group">
			<label for="bank_account_no">Bank Account No. :</label>
			<input required type="text" value="<?php echo $rows->bank_account_no; ?>" placeholder="Bank Account No. " class="form-control" id="bank_account_no" name="credentials[bank_account_no]">
			</div>
		  <div class="form-group">
			<label for="bank_account_name">Bank Account Name :</label>
			<input required type="text" value="<?php echo $rows->bank_account_name; ?>" placeholder="Bank Account Name " class="form-control" id="bank_account_name" name="credentials[bank_account_name]">
			</div>
		  <div class="form-group">
			<label for="no_listing_allowed">Number Listing Allowed :</label>
			<input <?php echo $disabled; ?> required value="<?php echo $rows->no_listing_allowed; ?>" type="text" placeholder="Number Listing Allowed " class="form-control" id="no_listing_allowed" name="credentials[no_listing_allowed]">
			</div>
			 <div class="form-group">
			  <label for="status">Status:</label>
			  <select <?php echo $disabled; ?> required class="form-control" id="status" name="credentials[status]">
				<option <?php echo $rows->status == "active" ? " selected " : false; ?> value="active">Active</option>
				<option <?php echo $rows->status == "inactive" ? " selected " : false; ?> value="inactive">Inactive</option>
				<option <?php echo $rows->status == "suspended" ? " selected " : false; ?> value="suspended">Suspended</option>
			  </select>
			</div>
			<input type="submit" class="btn btn-primary"  value="Update" />
		</form>
      </div>
    </div>
<?php endforeach; ?>
</div>
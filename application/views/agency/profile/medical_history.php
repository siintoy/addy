<?php 
	$evaluation_skill = (!empty($info[0]->evaluation_skill) ? explode("|",$info[0]->evaluation_skill) : null); 
	$Xillness = (!empty($info[0]->illness) ? explode("|",$info[0]->illness) : null); 
	$newArr = array();
	$newArr1 = array();
	foreach($illness as $oo){
		if(!@in_array($oo, $Xillness)){
			$newArr[] = $oo;
		}
	}

	
	foreach($eval as $ee){
		if(!@in_array($ee, $evaluation_skill)){
			$newArr1[] = $ee;
		}
	}
?>
<div class="form-group">
	<label for="allergies">Allergies (if any):</label>
	<input required type="text" value="<?php echo isset($info[0]->allergies) ? $info[0]->allergies : false ;?>" placeholder="Allergies (if any)"  class="form-control" id="allergies" name="personal_medical[allergies]" />
</div>

<div class="form-group">
	<label>Past and existing illnesses (including chronic ailments and illnesses requiring medication):</label> <br />
		
		<?php if(count($Xillness) > 0): 
			foreach($Xillness as $q): ?>
			<label><input name="personal_medical[illness][]" checked type="checkbox" value="<?php echo $q; ?>"> <?php echo $q; ?> </label><br />
		<?php endforeach; endif;  ?>
		
		<?php foreach($newArr as $q): ?>
			<label><input name="personal_medical[illness][]"  type="checkbox" value="<?php echo $q; ?>"> <?php echo $q; ?> </label><br />
		<?php endforeach; ?>
</div>

<div class="form-group">
	<label for="disabilities">Physical disablilities:</label>
	<textarea required type="text"  placeholder="Physical disablilities"  class="form-control" id="disabilities" name="personal_medical[disabilities]" ><?php echo isset($info[0]->disabilities) ? $info[0]->disabilities : false ;?></textarea>
</div>

<div class="form-group">
	<label for="dietary_restrictions">Dietary restrictions:</label>
	<textarea required type="text"  placeholder="Dietary restrictions"  class="form-control" id="dietary_restrictions" name="personal_medical[dietary_restrictions]" ><?php echo isset($info[0]->dietary_restrictions) ? $info[0]->dietary_restrictions : false ;?></textarea>
</div>

<div class="form-group">
	<label for="food_preferences">Food handling preferences:</label>
	<select  class="form-control" required name="personal_medical[food_preferences]" id="food_preferences">
		<?php foreach($food as $w): ?>
			<option  value="<?php echo $w; ?>"><?php echo $w; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<div class="form-group">
	<label for="rest_day">Preference for rest day per month:</label>
	<input value="<?php echo isset($info[0]->rest_day) ? $info[0]->rest_day : false ;?>" required type="text" onkeypress='return isNumberKey(event);' placeholder="Preference for rest day per month"  class="form-control" id="rest_day" name="personal_medical[rest_day]" />
</div>

<div class="form-group">
	<label for="others">Any other remarks:</label>
	<textarea required type="text"  placeholder="Any other remarks"  class="form-control" id="others" name="personal_medical[others]" ><?php echo isset($info[0]->others) ? $info[0]->others : false ;?></textarea>
</div>

<div class="form-group">
	<label>Please indicate the method(s) used to evaluate the FDW's skills:</label> <br />
	<?php if(count($evaluation_skill) > 0): foreach($evaluation_skill as $e): ?>
		<label><input name="personal_medical[evaluation_skill][]" checked type="checkbox" value="<?php echo $e; ?>"> <?php echo $e; ?> </label><br />
	<?php endforeach; endif; ?>
	
	<?php  foreach($newArr1 as $e): ?>
		<label><input name="personal_medical[evaluation_skill][]" type="checkbox" value="<?php echo $e; ?>"> <?php echo $e; ?> </label><br />
	<?php endforeach; ?>
</div>
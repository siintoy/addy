<div style="padding:10px;" class="row">
<?php if(isset($submit)): ?>
  <div class="<?php echo $class; ?>">
    <a href="#" class="close" data-dismiss="<?php echo $dismiss; ?>" aria-label="close">&times;</a>
    <strong><?php echo $errType; ?>!: </strong> <?php echo $msg; ?>.
  </div> 
<?php endif; ?>
  <!-- Modal content-->
    <div class="">
      <div class="">
         <form role="form" method="post" action="<?php echo base_url();?>index.php/admin/recruiter/new_recruiter">
		  <div class="form-group">
			<label for="username">Username:</label>
			<input required type="text"  placeholder="Username"  class="form-control" id="username" name="user[username]">
		  </div>
		  <div class="form-group">
			<label for="password">Password:</label>
			<input required type="password"  placeholder="Password"  class="form-control" id="password" name="user[password]">
		  </div>
		  <div class="form-group">
			<label for="name">Recruiter Name:</label>
			<input required type="text" placeholder="Recruiter Name"  class="form-control" id="name" name="credentials[name]">
		  </div>
		  <div class="form-group">
			<label for="address">Recruiter Address:</label>
			<input required type="text" placeholder="Recruiter Address"  class="form-control" id="address" name="credentials[address]"/>
		  </div>
		  <div class="form-group">
			<label for="phone">Recruiter Phone:</label>
			<input required type="text" placeholder="Recruiter Phone"  class="form-control" id="phone" name="credentials[phone]">
		  </div>
		  <div class="form-group">
			<label for="country">Country:</label>
			<select required name="credentials[country]" id="country" class="form-control">
				<?php foreach($country as $countries): ?>
				<option value="<?php echo $countries->code; ?>"><?php echo $countries->name; ?></option>
				<?php endforeach; ?>
			</select>
			</div>
		  <div class="form-group">
			<label for="contact_person">Contact Person:</label>
			<input required type="text" class="form-control" placeholder="Contact Person"   id="contact_person" name="credentials[contact_person]">
		  </div>
		  <div class="form-group">
			<label for="contact_person_no">Contact Person No.:</label>
			<input required type="text" class="form-control" placeholder="Contact Person No."  id="contact_person_no" name="credentials[contact_person_no]">
		  </div>
		  <div class="form-group">
			<label for="contact_person_email">Contact Person Email:</label>
			<input required type="email" class="form-control" placeholder="Contact Person Email" id="contact_person_email" name="credentials[contact_person_email]">
		  </div>
		  <div class="form-group">
			<label for="interview">Available Form of Interview: (Phone / Skype)</label>
			<input required type="text" placeholder=" Interview Phone Number / Skyper Username " class="form-control" id="interview" name="credentials[interview]"/>
			</div>
		  <div class="form-group">
			<label for="bus_reg_no">Recruiter Business Registration No.:</label>
			<input required type="text" placeholder="Recruiter Business Registration No." class="form-control" id="bus_reg_no" name="credentials[bus_reg_no]">
			</div>
		  <div class="form-group">
			<label for="url">Recruiter URL:</label>
			<input required type="text" placeholder="Recruiter URL" class="form-control" id="url" name="credentials[url]">
			</div>
		  <div class="form-group">
			<label for="bank_account_no">Bank Account No. :</label>
			<input required type="text" placeholder="Bank Account No. " class="form-control" id="bank_account_no" name="credentials[bank_account_no]">
			</div>
		  <div class="form-group">
			<label for="bank_account_name">Bank Account Name :</label>
			<input required type="text" placeholder="Bank Account Name " class="form-control" id="bank_account_name" name="credentials[bank_account_name]">
			</div>
		  <div class="form-group">
			<label for="no_listing_allowed">Number Booking Allowed :</label>
			<input required type="text" placeholder="Number Booking Allowed " class="form-control" id="no_booking_allowed" name="credentials[no_booking_allowed]">
			</div>
			 <div class="form-group">
			  <label for="status">Status:</label>
			  <select required class="form-control" id="status" name="credentials[status]">
				<option value="active">Active</option>
				<option value="inactive">Inactive</option>
				<option value="suspended">Suspended</option>
			  </select>
			</div>
			<input type="submit" class="btn btn-primary"  value="Submit" />
		</form>
      </div>
    </div>
</div>
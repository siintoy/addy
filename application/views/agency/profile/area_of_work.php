<?php 

	
	$arr1X = array(
			'care_of_infants_0to2' 		=> $info[0]->care_of_infants_0to2,
			'care_of_infants_3to5' 		=> $info[0]->care_of_infants_3to5,
			'care_of_infants_6to8' 		=> $info[0]->care_of_infants_6to8,
			'care_of_infants_9to11' 	=> $info[0]->care_of_infants_9to11,
			'care_of_infants_12to14' 	=> $info[0]->care_of_infants_12to14,
			'care_for_elderly' 			=> $info[0]->care_for_elderly,
			'care_for_disabled' 		=> $info[0]->care_for_disabled,
			'general_housework' 		=> $info[0]->general_housework,
			'cooking' 					=> $info[0]->cooking
			);
			
	$arr1 = array(
			'care_of_infants_0to2' 		=> 'Care of infants/children aged 0-2',
			'care_of_infants_3to5' 		=> 'Care of infants/children aged 3-5',
			'care_of_infants_6to8' 		=> 'Care of infants/children aged 6-8',
			'care_of_infants_9to11' 	=> 'Care of infants/children aged 9-11',
			'care_of_infants_12to14' 	=> 'Care of infants/children aged 12-14',
			'care_for_elderly' 			=> 'Care for elderly',
			'care_for_disabled' 		=> 'Care of disabled',
			'general_housework' 		=> 'General Housework',
			'cooking' 					=> 'Cooking'
			);
	
	$arr2X = array(
			'num_care_of_infants_0to2' 		=> $info[0]->num_care_of_infants_0to2,
			'num_care_of_infants_3to5' 		=> $info[0]->num_care_of_infants_3to5, 	
			'num_care_of_infants_6to8' 		=> $info[0]->num_care_of_infants_6to8, 	
			'num_care_of_infants_9to11' 	=> $info[0]->num_care_of_infants_9to11, 
			'num_care_of_infants_12to14' 	=> $info[0]->num_care_of_infants_12to14,
			'num_care_for_elderly' 			=> $info[0]->num_care_for_elderly, 		
			'num_care_for_disabled' 		=> $info[0]->num_care_for_disabled, 	
			'num_general_housework' 		=> $info[0]->num_general_housework, 	
			'num_cooking' 					=> $info[0]->num_cooking	
			);
	
	$arr2 = array(
			'num_care_of_infants_0to2' 		=> 'Care of infants/children aged 0-2',
			'num_care_of_infants_3to5' 		=> 'Care of infants/children aged 3-5',
			'num_care_of_infants_6to8' 		=> 'Care of infants/children aged 6-8',
			'num_care_of_infants_9to11' 	=> 'Care of infants/children aged 9-11',
			'num_care_of_infants_12to14' 	=> 'Care of infants/children aged 12-14',
			'num_care_for_elderly' 			=> 'Care for elderly',
			'num_care_for_disabled' 		=> 'Care of disabled',
			'num_general_housework' 		=> 'General Housework',
			'num_cooking' 					=> 'Cooking'
			);
	
	$arr3X = array(
			'rate_care_of_infants_0to2' 		=> $info[0]->rate_care_of_infants_0to2, 
			'rate_care_of_infants_3to5' 		=> $info[0]->rate_care_of_infants_3to5, 
			'rate_care_of_infants_6to8' 		=> $info[0]->rate_care_of_infants_6to8, 
			'rate_care_of_infants_9to11' 		=> $info[0]->rate_care_of_infants_9to11,
			'rate_care_of_infants_12to14' 		=> $info[0]->rate_care_of_infants_12to14,
			'rate_care_for_elderly' 			=> $info[0]->rate_care_for_elderly,	
			'rate_care_for_disabled' 			=> $info[0]->rate_care_for_disabled, 	
			'rate_general_housework' 			=> $info[0]->rate_general_housework, 	
			'rate_cooking' 						=> $info[0]->rate_cooking		
			);
			
	$arr3 = array(
			'rate_care_of_infants_0to2' 		=> 'Care of infants/children aged 0-2',
			'rate_care_of_infants_3to5' 		=> 'Care of infants/children aged 3-5',
			'rate_care_of_infants_6to8' 		=> 'Care of infants/children aged 6-8',
			'rate_care_of_infants_9to11' 		=> 'Care of infants/children aged 9-11',
			'rate_care_of_infants_12to14' 	=> 'Care of infants/children aged 12-14',
			'rate_care_for_elderly' 				=> 'Care for elderly',
			'rate_care_for_disabled' 			=> 'Care of disabled',
			'rate_general_housework' 			=> 'General Housework',
			'rate_cooking' 							=> 'Cooking'
			);
	
	$lang = array(
				"English",
				"Mandarin/Chinese-Dialect",
				"Basaha Indonesia/Malayia",
				"Tamil",
				"Malayan",
				"Telugu",
				"Burmese",
				"Tagalog",
				"Thai",
				"Hindi"
				);
?>
<div class="form-group">
	<label for="language">Language:</label>
	<select required class="form-control" required name="personal_area_of_work[language]" id="language">
		<?php foreach($lang as $io): ?>
			<option <?php echo $info[0]->language == $io ? " selected " : false; ?> value="<?php echo $io; ?>"><?php echo $io; ?></option>
		<?php endforeach; ?>
	</select>
</div>

<label>Willingness:</label> <br /> <br />

<?php $x = 0; foreach($arr1 as $key=>$a): ?>
<div class="form-group">
	<label for="<?php echo $key; ?>"><?php echo $a; ?>:</label>
	<select required class="form-control" required name="personal_area_of_work[<?php echo $key; ?>]" id="<?php echo $key; ?>">
		<option <?php echo $arr1X[$key] == 1 ? " selected " : false; ?> value="1">Yes</option>
		<option <?php echo $arr1X[$key] == 0 ? " selected " : false; ?> value="0">No</option>
	</select>
</div>
<?php $x++; endforeach; ?>

<div class="form-group">
	<label for="language_spoken">Language Spoken:</label>
	<select required class="form-control" required name="personal_area_of_work[language_spoken]" id="language_spoken">
		<?php $x = 0; foreach($lang as $key=>$r): ?>
			<option value="<?php echo $r; ?>"><?php echo $r; ?></option>
		<?php $x++; endforeach; ?>
	</select>

</div>

<div class="form-group">
	<label for="other_skills">Other Skills if any:</label>
	<textarea required placeholder="Other Skills if any"  class="form-control" id="other_skills" name="personal_area_of_work[other_skills]" ><?php echo isset($info[0]->other_skills) ? $info[0]->other_skills : false ;?></textarea>
</div>

<label>Experience in Years:</label> <br /> <br />

<?php $x = 0; foreach($arr2 as $key=>$b): ?>
<div class="form-group">
	<label for="<?php echo $key; ?>"><?php echo $b; ?>:</label>
	<select required class="form-control" required name="personal_area_of_work[<?php echo $key; ?>]" id="<?php echo $key; ?>">
		<?php for($x = 0;$x <= 100; $x++): ?>
			<option <?php echo $arr2X[$key] == $x ? " selected " : false; ?> value="<?php echo $x; ?>"><?php echo $x; ?></option>
		<?php endfor; ?>
	</select>
</div>
<?php $x++; endforeach; ?>
	
<label>Assessment & Observation:</label> <br /> <br />

<?php $x = 0; foreach($arr3 as $key=>$c): ?>
<div class="form-group">
	<label for="<?php echo $key; ?>"><?php echo $c; ?> (Ratings):</label>
	<select required class="form-control" required name="personal_area_of_work[<?php echo $key; ?>]" id="<?php echo $key; ?>">
		<?php for($y = 1;$y <= 5; $y++): ?>
			<option <?php echo $arr3X[$key] == $y ? " selected " : false; ?> value="<?php echo $y; ?>"><?php echo $y; ?></option>
		<?php endfor; ?>
	</select>
</div>
<?php $x++; endforeach; ?>


<div class="container">

<div style="padding:5px;border:1px solid #ccc;  "class="col-xs-12">
	<h4>Profile Search</h4>
	<div class="form-group">
	 <form role="form" method="post" action="<?php echo base_url();?>index.php/main/search_profiles?id=<?php echo $id;?>">

		<table class="table table-nonfluid">
			<tr>
				<td>
					<label for="age">Age:</label>
					<select class="form-control"  name="age" id="age">
						<option  value="">Please Select Age</option>
						<?php for($x = 17;$x <= 40;$x=($x + 5)): ?>
							<option  value="<?php echo (($x + 5) < 40 ? $x."-".($x + 5) : "40+"); ?>"><?php echo (($x + 5) < 40 ? $x." - ".($x + 5) : "Above 40"); ?></option>
						<?php endfor; ?>
					</select>
				</td>
				<td>
					<label for="education">Education level:</label>
					<select  class="form-control"  name="education" id="education">
							<option  value="">Please Select Education</option>
						<?php foreach($educ as $w): ?>
							<option  value="<?php echo $w; ?>"><?php echo $w; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label for="religion">Religion:</label>
					<select  class="form-control"  name="religion" id="religion">
							<option  value="">Please Select Religion</option>
						<?php foreach($rel as $e): ?>
							<option  value="<?php echo $e; ?>"><?php echo $e; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
				<td>
					<label for="marital_status">Marital status:</label>
					<select  class="form-control"  name="marital_status" id="marital_status">
							<option  value="">Please Select Education</option>
						<?php foreach($stat as $q): ?>
							<option  value="<?php echo $q; ?>"><?php echo $q; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right"><input type="submit" name="search" class="btn btn-primary" value="Search Profile" /></td>
			</tr>
		</table>
	</form>
		
	</div>
</div>
<div style="padding:5px;"class="col-xs-12">
	<h3>Latest Profile</h3>
	<div style="padding:10px;"class="col-xs-13">
		<?php foreach($latest as $l): ?>
			<div class="lastest_profile" style="margin:5px;border:1px solid #ccc; height:200px;width:15%;">
				<a href="<?php echo site_url("main/view_profile?profile_id=".$l->id."&id=".$id."");?>">
					<img class="img-thumbnail" style="height:100px;width:100%" src="<?php echo base_url(); ?>uploads/<?php echo $l->picture; ?>"/></a>
				<div style="padding:5px;"> 
					<?php echo $l->name; ?> <br> 
					Age: <?php echo $l->age; ?> <br> 
					<?php echo ucwords($l->religion); ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<?php foreach($code as $rows): 
	$profile = $this->mod_profile->getProfileByCountry($rows->code, $this->input->get("id"));
?>
 <div class="col-xs-12">
		<div style="border:1px solid #ccc"  class="panel panel-default">
			<div style="border-bottom:1px solid #ccc"  class="panel-heading">
				<?php echo $rows->name;?>	
				<div style="float:right;">
					<a href="#"><?php echo number_format(($rows->count - 0 < 0 ? 0 : ($rows->count - 0) )); ?> more <?php echo $rows->name; ?> profile(s), click for more... </a>
				</div>
			</div>
			<div class="panel-body">
				<div style="padding:10px;">
					<?php foreach($profile as $prof): ?>
						<div class="lastest_profile" style="margin:5px;border:1px solid #ccc; height:200px;width:15%;">
						<a href="<?php echo site_url("main/view_profile?profile_id=".$prof->id."&id=".$id."");?>">
							<img class="img-thumbnail"  style="height:100px;width:100%" src="<?php echo base_url(); ?>uploads/<?php echo $prof->picture; ?>"/></a>
							<div style="padding:5px;"> 
								<?php echo $prof->name; ?> <br> 
								Age: <?php echo $prof->age; ?> <br> 
								<?php echo ucwords($prof->religion); ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
</div>
<?php endforeach; ?>
</div>
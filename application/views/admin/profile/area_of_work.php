<?php 
	$arr1 = array(
			'care_of_infants_0to2' 		=> 'Care of infants/children aged 0-2',
			'care_of_infants_3to5' 		=> 'Care of infants/children aged 3-5',
			'care_of_infants_6to8' 		=> 'Care of infants/children aged 6-8',
			'care_of_infants_9to11' 	=> 'Care of infants/children aged 9-11',
			'care_of_infants_12to14' 	=> 'Care of infants/children aged 12-14',
			'care_for_elderly' 				=> 'Care for elderly',
			'care_for_disabled' 			=> 'Care of disabled',
			'general_housework' 			=> 'General Housework',
			'cooking' 						=> 'Cooking'
			);
	
	$arr2 = array(
			'num_care_of_infants_0to2' 		=> 'Care of infants/children aged 0-2',
			'num_care_of_infants_3to5' 		=> 'Care of infants/children aged 3-5',
			'num_care_of_infants_6to8' 		=> 'Care of infants/children aged 6-8',
			'num_care_of_infants_9to11' 		=> 'Care of infants/children aged 9-11',
			'num_care_of_infants_12to14' 	=> 'Care of infants/children aged 12-14',
			'num_care_for_elderly' 				=> 'Care for elderly',
			'num_care_for_disabled' 			=> 'Care of disabled',
			'num_general_housework' 			=> 'General Housework',
			'num_cooking' 							=> 'Cooking'
			);
	
	$arr3 = array(
			'rate_care_of_infants_0to2' 		=> 'Care of infants/children aged 0-2',
			'rate_care_of_infants_3to5' 		=> 'Care of infants/children aged 3-5',
			'rate_care_of_infants_6to8' 		=> 'Care of infants/children aged 6-8',
			'rate_care_of_infants_9to11' 		=> 'Care of infants/children aged 9-11',
			'rate_care_of_infants_12to14' 	=> 'Care of infants/children aged 12-14',
			'rate_care_for_elderly' 				=> 'Care for elderly',
			'rate_care_for_disabled' 			=> 'Care of disabled',
			'rate_general_housework' 			=> 'General Housework',
			'rate_cooking' 							=> 'Cooking'
			);
	
	$lang = array(
				"English",
				"Mandarin/Chinese-Dialect",
				"Basaha Indonesia/Malayia",
				"Tamil",
				"Malayan",
				"Telugu",
				"Burmese",
				"Tagalog",
				"Thai",
				"Hindi"
				);
?>
<label>Willingness:</label> <br /> <br />

<?php $x = 0; foreach($arr1 as $key=>$a): ?>
<div class="form-group">
	<label for="<?php echo $key; ?>"><?php echo $a; ?>:</label>
	<select required class="form-control" required name="personal_area_of_work[<?php echo $key; ?>]" id="<?php echo $key; ?>">
		<option value="1">Yes</option>
		<option value="0">No</option>
	</select>
</div>
<?php $x++; endforeach; ?>

<div class="form-group">
	<label for="language_spoken">Language Spoken:</label>
	<select required class="form-control" required name="personal_area_of_work[language_spoken]" id="language_spoken">
		<?php $x = 0; foreach($lang as $key=>$r): ?>
			<option value="<?php echo $r; ?>"><?php echo $r; ?></option>
		<?php $x++; endforeach; ?>
	</select>

</div>

<div class="form-group">
	<label for="other_skills">Other Skills if any:</label>
	<textarea required placeholder="Other Skills if any"  class="form-control" id="other_skills" name="personal_area_of_work[other_skills]" ></textarea>
</div>

<label>Experience in Years:</label> <br /> <br />

<?php $x = 0; foreach($arr2 as $key=>$b): ?>
<div class="form-group">
	<label for="<?php echo $key; ?>"><?php echo $b; ?>:</label>
	<select required class="form-control" required name="personal_area_of_work[<?php echo $key; ?>]" id="<?php echo $key; ?>">
		<?php for($x = 0;$x <= 100; $x++): ?>
			<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
		<?php endfor; ?>
	</select>
</div>
<?php $x++; endforeach; ?>
	
<label>Assessment & Observation:</label> <br /> <br />

<?php $x = 0; foreach($arr3 as $key=>$c): ?>
<div class="form-group">
	<label for="<?php echo $key; ?>"><?php echo $c; ?> (Ratings):</label>
	<select required class="form-control" required name="personal_area_of_work[<?php echo $key; ?>]" id="<?php echo $key; ?>">
		<?php for($y = 1;$y <= 5; $y++): ?>
			<option value="<?php echo $y; ?>"><?php echo $y; ?></option>
		<?php endfor; ?>
	</select>
</div>
<?php $x++; endforeach; ?>


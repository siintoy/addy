-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2016 at 03:22 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `profilexchange`
--

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `address` text,
  `phone` varchar(11) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `contact_person` varchar(32) DEFAULT NULL,
  `contact_person_no` varchar(11) DEFAULT NULL,
  `contact_person_email` varchar(32) DEFAULT NULL,
  `interview` varchar(12) DEFAULT NULL,
  `bus_reg_no` varchar(11) DEFAULT NULL,
  `url` text,
  `bank_account_no` varchar(30) DEFAULT NULL,
  `bank_account_name` varbinary(32) DEFAULT NULL,
  `no_listing_allowed` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`id`, `userid`, `name`, `address`, `phone`, `country`, `contact_person`, `contact_person_no`, `contact_person_email`, `interview`, `bus_reg_no`, `url`, `bank_account_no`, `bank_account_name`, `no_listing_allowed`, `status`) VALUES
(1, 1, 'Agency Namel', 'Agency Address', 'Agency Phon', 'PH', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Agency Busi', 'Agency URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(2, 2, 'Agency Name', 'Agency Address', 'Agency Phon', 'US', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Agency Busi', 'Agency URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(3, 3, 'Agency Name', 'Agency Address', 'Agency Phon', 'PH', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Agency Busi', 'Agency URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(4, 4, 'Agency Name', 'Agency Address', 'Agency Phon', 'US', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Agency Busi', 'Agency URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(7, 7, 'My new Agency', '04-c Kampupot St. Puwang Wawa, Taguig City', '09468965897', 'PH', 'Jan Christian', '09468965897', 'christianaristain@gmail.com', '15872585', '5458774', 'http://localhost/addy/index.php/admin/agency/new_agency', '55555555', 0x62616e65206e616d65, 12, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(5) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AX', 'Åland Islands'),
(3, 'AL', 'Albania'),
(4, 'DZ', 'Algeria'),
(5, 'AS', 'American Samoa'),
(6, 'AD', 'Andorra'),
(7, 'AO', 'Angola'),
(8, 'AI', 'Anguilla'),
(9, 'AQ', 'Antarctica'),
(10, 'AG', 'Antigua and Barbuda'),
(11, 'AR', 'Argentina'),
(12, 'AM', 'Armenia'),
(13, 'AW', 'Aruba'),
(14, 'AU', 'Australia'),
(15, 'AT', 'Austria'),
(16, 'AZ', 'Azerbaijan'),
(17, 'BS', 'Bahamas'),
(18, 'BH', 'Bahrain'),
(19, 'BD', 'Bangladesh'),
(20, 'BB', 'Barbados'),
(21, 'BY', 'Belarus'),
(22, 'BE', 'Belgium'),
(23, 'BZ', 'Belize'),
(24, 'BJ', 'Benin'),
(25, 'BM', 'Bermuda'),
(26, 'BT', 'Bhutan'),
(27, 'BO', 'Bolivia'),
(28, 'BA', 'Bosnia and Herzegovina'),
(29, 'BW', 'Botswana'),
(30, 'BV', 'Bouvet Island'),
(31, 'BR', 'Brazil'),
(32, 'IO', 'British Indian Ocean Territory'),
(33, 'BN', 'Brunei Darussalam'),
(34, 'BG', 'Bulgaria'),
(35, 'BF', 'Burkina Faso'),
(36, 'BI', 'Burundi'),
(37, 'KH', 'Cambodia'),
(38, 'CM', 'Cameroon'),
(39, 'CA', 'Canada'),
(40, 'CV', 'Cape Verde'),
(41, 'KY', 'Cayman Islands'),
(42, 'CF', 'Central African Republic'),
(43, 'TD', 'Chad'),
(44, 'CL', 'Chile'),
(45, 'CN', 'China'),
(46, 'CX', 'Christmas Island'),
(47, 'CC', 'Cocos (Keeling) Islands'),
(48, 'CO', 'Colombia'),
(49, 'KM', 'Comoros'),
(50, 'CG', 'Congo'),
(51, 'CD', 'Congo, The Democratic Republic'),
(52, 'CK', 'Cook Islands'),
(53, 'CR', 'Costa Rica'),
(54, 'CI', 'Cote D''ivoire'),
(55, 'HR', 'Croatia'),
(56, 'CU', 'Cuba'),
(57, 'CY', 'Cyprus'),
(58, 'CZ', 'Czech Republic'),
(59, 'DK', 'Denmark'),
(60, 'DJ', 'Djibouti'),
(61, 'DM', 'Dominica'),
(62, 'DO', 'Dominican Republic'),
(63, 'EC', 'Ecuador'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Faroe Islands'),
(72, 'FJ', 'Fiji'),
(73, 'FI', 'Finland'),
(74, 'FR', 'France'),
(75, 'GF', 'French Guiana'),
(76, 'PF', 'French Polynesia'),
(77, 'TF', 'French Southern Territories'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germany'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GG', 'Guernsey'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard Island and Mcdonald Isla'),
(96, 'VA', 'Holy See (Vatican City State)'),
(97, 'HN', 'Honduras'),
(98, 'HK', 'Hong Kong'),
(99, 'HU', 'Hungary'),
(100, 'IS', 'Iceland'),
(101, 'IN', 'India'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran, Islamic Republic of'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IM', 'Isle of Man'),
(107, 'IL', 'Israel'),
(108, 'IT', 'Italy'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JE', 'Jersey'),
(112, 'JO', 'Jordan'),
(113, 'KZ', 'Kazakhstan'),
(114, 'KE', 'Kenya'),
(115, 'KI', 'Kiribati'),
(116, 'KP', 'Korea, Democratic People''s Rep'),
(117, 'KR', 'Korea, Republic of'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People''s Democratic Republ'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macao'),
(130, 'MK', 'Macedonia, The Former Yugoslav'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'YT', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States o'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestinian Territory, Occupie'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'SH', 'Saint Helena'),
(185, 'KN', 'Saint Kitts and Nevis'),
(186, 'LC', 'Saint Lucia'),
(187, 'PM', 'Saint Pierre and Miquelon'),
(188, 'VC', 'Saint Vincent and The Grenadin'),
(189, 'WS', 'Samoa'),
(190, 'SM', 'San Marino'),
(191, 'ST', 'Sao Tome and Principe'),
(192, 'SA', 'Saudi Arabia'),
(193, 'SN', 'Senegal'),
(194, 'RS', 'Serbia'),
(195, 'SC', 'Seychelles'),
(196, 'SL', 'Sierra Leone'),
(197, 'SG', 'Singapore'),
(198, 'SK', 'Slovakia'),
(199, 'SI', 'Slovenia'),
(200, 'SB', 'Solomon Islands'),
(201, 'SO', 'Somalia'),
(202, 'ZA', 'South Africa'),
(203, 'GS', 'South Georgia and The South Sa'),
(204, 'ES', 'Spain'),
(205, 'LK', 'Sri Lanka'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan, Province of China'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TL', 'Timor-leste'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad and Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks and Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States Minor Outlying I'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Viet Nam'),
(238, 'VG', 'Virgin Islands, British'),
(239, 'VI', 'Virgin Islands, U.S.'),
(240, 'WF', 'Wallis and Futuna'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'ZM', 'Zambia'),
(244, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `news` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news`) VALUES
(1, '(This will be edited manually by Administrator, all agency and recruiter will see this on their homepage),\r\n\r\nEtiam nisi lorem, posuere at turpis at, fringilla efficitur quam. Suspendisse vitae lacus ac lectus facilisis ornare. Vivamus vitae pulvinar nisi, in vehicula elit. Praesent iaculis ante tellus, eu mattis lectus suscipit sit amet. Sed congue accumsan nunc in iaculis. \r\n\r\nSed malesuada elit turpis, eu egestas eros rhoncus non. Sed pulvinar euismod libero sit amet scelerisque. Vestibulum ante felis, condimentum in vulputate id, tempor eu nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. \r\n\r\nEtiam condimentum augue vitae nulla suscipit eleifend. Phasellus consectetur volutpat nulla, cursus facilisis elit vulputate sed. Mauris in semper sem. Interdum et malesuada fames ac ante ipsum primis in faucibus.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `profile_area_of_work`
--

CREATE TABLE `profile_area_of_work` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `care_of_infants_0to2` tinyint(4) DEFAULT '0',
  `care_of_infants_3to5` tinyint(4) DEFAULT '0',
  `care_of_infants_6to8` tinyint(4) DEFAULT '0',
  `care_of_infants_9to11` tinyint(4) DEFAULT '0',
  `care_of_infants_12to14` tinyint(4) DEFAULT '0',
  `care_for_elderly` tinyint(4) DEFAULT '0',
  `care_for_disabled` tinyint(4) DEFAULT '0',
  `general_housework` tinyint(4) DEFAULT '0',
  `cooking` tinyint(4) DEFAULT '0',
  `language_spoken` text,
  `other_skills` text,
  `num_care_of_infants_0to2` int(11) DEFAULT NULL,
  `num_care_of_infants_3to5` int(11) DEFAULT NULL,
  `num_care_of_infants_6to8` int(11) DEFAULT NULL,
  `num_care_of_infants_9to11` int(11) DEFAULT NULL,
  `num_care_of_infants_12to14` int(11) DEFAULT NULL,
  `num_care_for_elderly` int(11) DEFAULT NULL,
  `num_care_for_disabled` int(11) DEFAULT NULL,
  `num_general_housework` int(11) DEFAULT NULL,
  `num_cooking` int(11) DEFAULT NULL,
  `rate_care_of_infants_0to2` int(11) DEFAULT NULL,
  `rate_care_of_infants_3to5` int(11) DEFAULT NULL,
  `rate_care_of_infants_6to8` int(11) DEFAULT NULL,
  `rate_care_of_infants_9to11` int(11) DEFAULT NULL,
  `rate_care_of_infants_12to14` int(11) DEFAULT NULL,
  `rate_care_for_elderly` int(11) DEFAULT NULL,
  `rate_care_for_disabled` int(11) DEFAULT NULL,
  `rate_general_housework` int(11) DEFAULT NULL,
  `rate_cooking` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_area_of_work`
--

INSERT INTO `profile_area_of_work` (`id`, `profile_id`, `care_of_infants_0to2`, `care_of_infants_3to5`, `care_of_infants_6to8`, `care_of_infants_9to11`, `care_of_infants_12to14`, `care_for_elderly`, `care_for_disabled`, `general_housework`, `cooking`, `language_spoken`, `other_skills`, `num_care_of_infants_0to2`, `num_care_of_infants_3to5`, `num_care_of_infants_6to8`, `num_care_of_infants_9to11`, `num_care_of_infants_12to14`, `num_care_for_elderly`, `num_care_for_disabled`, `num_general_housework`, `num_cooking`, `rate_care_of_infants_0to2`, `rate_care_of_infants_3to5`, `rate_care_of_infants_6to8`, `rate_care_of_infants_9to11`, `rate_care_of_infants_12to14`, `rate_care_for_elderly`, `rate_care_for_disabled`, `rate_general_housework`, `rate_cooking`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(8, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(9, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(10, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(11, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(12, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(13, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(14, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(15, 15, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(16, 16, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(17, 17, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(18, 18, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(19, 19, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(20, 20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(21, 21, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(22, 22, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(23, 23, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(24, 24, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(25, 25, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(26, 26, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(27, 27, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(28, 106, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(29, 107, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(30, 108, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(31, 109, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(32, 110, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(33, 111, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(34, 112, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(35, 113, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(36, 114, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(37, 115, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(38, 116, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(39, 117, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(40, 118, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(41, 119, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(42, 120, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(43, 121, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(44, 122, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(45, 123, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(46, 124, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(47, 125, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Tagalog', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_employment_history`
--

CREATE TABLE `profile_employment_history` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `employer` text,
  `date_from` text,
  `date_to` text,
  `country` text,
  `work_duties` text,
  `remarks` text,
  `emp_feedback` text,
  `prev_work_sg` tinyint(4) DEFAULT NULL,
  `availability_fdw` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_employment_history`
--

INSERT INTO `profile_employment_history` (`id`, `profile_id`, `employer`, `date_from`, `date_to`, `country`, `work_duties`, `remarks`, `emp_feedback`, `prev_work_sg`, `availability_fdw`) VALUES
(1, 123, 'Employer:1', '2016-06-01', '2016-06-24', NULL, 'dsa', '231', 'aa', 1, '["FDW is not available for interview"]'),
(2, 124, 'Employer:1', '2016-06-01', '2016-06-24', NULL, 'dsa', '231', 'aa', 1, '["FDW is not available for interview"]'),
(3, 125, 'g', '2016-06-24', '2016-06-24', NULL, 'd', 'a', 'a', 1, '["FDW is not available for interview","FDW can be interviewed in person"]');

-- --------------------------------------------------------

--
-- Table structure for table `profile_medical_history`
--

CREATE TABLE `profile_medical_history` (
  `int` int(11) NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `allergies` text,
  `illness` text COMMENT 'Past and existing illnesses (including chronic ailments and illnesses requiring medication):\r\nCHECKBOX (Mental illness, Tuberculosis, Epilepsy, Heart disease, Asthma, Malaria, Diabetes, Operations, Hypertension, Others)',
  `disabilities` text COMMENT 'Physical disablilities',
  `dietary_restrictions` text,
  `food_preferences` text COMMENT 'DROPDOWN (No pork, No beef, Vegetarian, Others)',
  `rest_day` int(11) DEFAULT NULL COMMENT 'Preference for rest day per month',
  `others` text,
  `evaluation_skill` text COMMENT 'Please indicate the method(s) used to evaluate the FDW''s skills (can tick more than one):'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_medical_history`
--

INSERT INTO `profile_medical_history` (`int`, `profile_id`, `allergies`, `illness`, `disabilities`, `dietary_restrictions`, `food_preferences`, `rest_day`, `others`, `evaluation_skill`) VALUES
(1, 1, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(2, 2, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(3, 3, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(4, 4, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(5, 5, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(6, 6, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(7, 7, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(8, 8, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(9, 9, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(10, 10, 'Allergies (if any):', 'Mental illness|Tuberculosis|Epilepsy|Heart disease|Asthma|Malaria|Others', 'Physical disablilities:', 'restrictions', 'No beef', 2, 'Any other remarks:', 'Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA|Interviewed by overseas training centre/EA'),
(11, 11, '', '', '', '', 'No pork', 0, '', ''),
(12, 12, '', '', '', '', 'No pork', 0, '', ''),
(13, 13, '', '', '', '', 'No pork', 0, '', ''),
(14, 14, '', '', '', '', 'No pork', 0, '', ''),
(15, 15, '', '', '', '', 'No pork', 0, '', ''),
(16, 16, '', '', '', '', 'No pork', 0, '', ''),
(17, 17, '', '', '', '', 'No pork', 0, '', ''),
(18, 18, '', '', '', '', 'No pork', 0, '', ''),
(19, 19, '', '', '', '', 'No pork', 0, '', ''),
(20, 20, '', '', '', '', 'No pork', 0, '', ''),
(21, 21, '', '', '', '', 'No pork', 0, '', ''),
(22, 22, '', '', '', '', 'No pork', 0, '', ''),
(23, 23, '', '', '', '', 'No pork', 0, '', ''),
(24, 24, '', '', '', '', 'No pork', 0, '', ''),
(25, 25, '', '', '', '', 'No pork', 0, '', ''),
(26, 26, '', '', '', '', 'No pork', 0, '', ''),
(27, 27, '', '', '', '', 'No pork', 0, '', ''),
(28, 106, '', '', '', '', 'No pork', 0, '', ''),
(29, 107, '', '', '', '', 'No pork', 0, '', ''),
(30, 108, '', '', '', '', 'No pork', 0, '', ''),
(31, 109, '', '', '', '', 'No pork', 0, '', ''),
(32, 110, '', '', '', '', 'No pork', 0, '', ''),
(33, 111, '', '', '', '', 'No pork', 0, '', ''),
(34, 112, '', '', '', '', 'No pork', 0, '', ''),
(35, 113, '', '', '', '', 'No pork', 0, '', ''),
(36, 114, '', '', '', '', 'No pork', 0, '', ''),
(37, 115, '', '', '', '', 'No pork', 0, '', ''),
(38, 116, '', '', '', '', 'No pork', 0, '', ''),
(39, 117, '', '', '', '', 'No pork', 0, '', ''),
(40, 118, '', '', '', '', 'No pork', 0, '', ''),
(41, 119, '', '', '', '', 'No pork', 0, '', ''),
(42, 120, '', '', '', '', 'No pork', 0, '', ''),
(43, 121, '', '', '', '', 'No pork', 0, '', ''),
(44, 122, '', '', '', '', 'No pork', 0, '', ''),
(45, 123, '', '', '', '', 'No pork', 0, '', ''),
(46, 124, '', '', '', '', 'No pork', 0, '', ''),
(47, 125, '', '', '', '', 'No pork', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `profile_personal_info`
--

CREATE TABLE `profile_personal_info` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `profile_owner` int(11) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(32) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `nationality` varchar(32) DEFAULT NULL,
  `home_address` text,
  `airport` varchar(32) DEFAULT NULL,
  `home_number` varchar(32) DEFAULT NULL,
  `religion` varchar(32) DEFAULT NULL COMMENT 'CHRISTIANITY, ISLAM, HINDUISM, TAOISM, BUDDHISM, SHINTO, SIKHISM, JUDAISM, AGNOSTIC, ATHEIST',
  `education` varchar(32) DEFAULT NULL,
  `no_of_siblings` int(11) DEFAULT NULL,
  `marital_status` varchar(32) DEFAULT NULL COMMENT 'SINGLE, IN A RELATIONSHIP, ENGAGED, MARRIED, DIVORCED, WIDOWED',
  `no_of_children` int(11) DEFAULT NULL,
  `age_of_childing` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_personal_info`
--

INSERT INTO `profile_personal_info` (`id`, `name`, `profile_owner`, `date_of_birth`, `place_of_birth`, `age`, `height`, `weight`, `nationality`, `home_address`, `airport`, `home_number`, `religion`, `education`, `no_of_siblings`, `marital_status`, `no_of_children`, `age_of_childing`) VALUES
(1, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(2, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(3, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(4, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(5, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(6, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(7, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(8, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(9, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(10, 'Name:', NULL, '2016-06-01', 'Place of Birth:', 24, 12, 121, 'Filipino', '04c Kampupot St. Puwang Wawa, Taguig City', 'NAIA', '09468965897', 'CHRISTIANITY', 'COMPLETED PRIMARY SCHOOL', 1, 'ENGAGED', 1, '12'),
(11, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(12, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(13, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(14, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(15, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(16, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(17, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(18, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(19, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(20, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(21, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(22, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(23, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(24, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(25, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(26, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(27, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(28, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(29, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(30, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(31, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(32, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(33, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(34, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(35, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(36, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(37, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(38, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(39, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(40, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(41, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(42, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(43, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(44, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(45, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(46, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(47, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(48, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(49, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(50, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(51, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(52, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(53, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(54, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(55, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(56, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(57, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(58, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(59, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(60, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(61, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(62, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(63, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(64, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(65, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(66, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(67, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(68, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(69, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(70, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(71, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(72, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(73, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(74, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(75, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(76, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(77, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(78, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(79, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(80, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(81, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(82, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(83, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(84, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(85, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(86, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(87, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(88, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(89, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(90, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(91, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(92, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(93, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(94, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(95, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(96, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(97, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(98, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(99, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(100, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(101, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(102, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(103, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(104, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(105, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(106, 'das', NULL, '2016-06-24', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(107, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(108, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(109, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(110, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(111, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(112, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(113, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(114, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(115, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(116, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(117, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(118, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(119, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(120, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(121, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(122, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(123, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(124, '', NULL, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, ''),
(125, '', 2, '0000-00-00', '', 0, 0, 0, '', '', '', '', '', '', 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `recruiter`
--

CREATE TABLE `recruiter` (
  `id` int(11) NOT NULL,
  `userid` int(15) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `address` text,
  `phone` varchar(11) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `contact_person` varchar(32) DEFAULT NULL,
  `contact_person_no` varchar(11) DEFAULT NULL,
  `contact_person_email` varchar(32) DEFAULT NULL,
  `interview` varchar(12) DEFAULT NULL,
  `bus_reg_no` varchar(11) DEFAULT NULL,
  `url` text,
  `bank_account_no` varchar(30) DEFAULT NULL,
  `bank_account_name` varbinary(32) DEFAULT NULL,
  `no_booking_allowed` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiter`
--

INSERT INTO `recruiter` (`id`, `userid`, `name`, `address`, `phone`, `country`, `contact_person`, `contact_person_no`, `contact_person_email`, `interview`, `bus_reg_no`, `url`, `bank_account_no`, `bank_account_name`, `no_booking_allowed`, `status`) VALUES
(1, 1, 'Recruiter Name', 'Recruiter Address', 'Recruiter P', 'PH', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Recruiter B', 'Recruiter URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(2, 3, '1', 'Recruiter Address', 'Recruiter P', 'US', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Recruiter B', 'Recruiter URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(3, 2, 'Recruiter Name', 'Recruiter Address', 'Recruiter P', 'PH', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Recruiter B', 'Recruiter URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(4, 3, 'Recruiter Name', 'Recruiter Address', 'Recruiter P', 'US', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Recruiter B', 'Recruiter URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(5, 4, 'Recruiter Name', 'Recruiter Address', 'Recruiter P', 'US', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Recruiter B', 'Recruiter URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active'),
(6, 5, 'Recruiter Name', 'Recruiter Address', 'Recruiter P', 'PH', 'Contact Person', 'Contact Per', 'dasdas@sadsa.com', ' Interview P', 'Recruiter B', 'Recruiter URL', 'Bank Account No. ', 0x42616e6b204163636f756e74204e616d6520, 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `user_type`, `status`) VALUES
(1, 'Username', 'dc647eb65e6711e155375218212b3964', 2, 1),
(2, 'Usernamed', 'dc647eb65e6711e155375218212b3964', 2, 1),
(3, 'Usernames', 'dc647eb65e6711e155375218212b3964', 2, 0),
(4, 'Usernamedasdas', 'dc647eb65e6711e155375218212b3964', 2, 1),
(5, 'Usernamedasd', 'dc647eb65e6711e155375218212b3964', 2, 1),
(6, 'haha', 'dc647eb65e6711e155375218212b3964', 2, 1),
(7, 'aeonflux', '7cf253e55693c11aacd6203042d8b19d', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_area_of_work`
--
ALTER TABLE `profile_area_of_work`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_employment_history`
--
ALTER TABLE `profile_employment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_medical_history`
--
ALTER TABLE `profile_medical_history`
  ADD PRIMARY KEY (`int`);

--
-- Indexes for table `profile_personal_info`
--
ALTER TABLE `profile_personal_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recruiter`
--
ALTER TABLE `recruiter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `profile_area_of_work`
--
ALTER TABLE `profile_area_of_work`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `profile_employment_history`
--
ALTER TABLE `profile_employment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `profile_medical_history`
--
ALTER TABLE `profile_medical_history`
  MODIFY `int` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `profile_personal_info`
--
ALTER TABLE `profile_personal_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT for table `recruiter`
--
ALTER TABLE `recruiter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class agency extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $userid = "";
	private $settings = array();
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('isLoggedin')){
			redirect("login");
		}elseif($this->session->userdata("role") == 1){
			redirect("admin");
		}
		$this->load->helper(array('form', 'url'));
		$this->userid = $this->session->userdata("userid");
	}
	 
	public function index($data = array())
	{
		$var1  						= array();
		$content  					= array();
		$content['pageHeader'] 		= "Frequest Asked Questions";
		$content['id'] 				= $this->userid;
		$var1['id']					= $this->userid;
		if($this->input->post()){
			$news = $this->input->post("faqs");
			
			$this->db->where("agency_id", $this->userid);
			$this->db->update("faqs", array("faqs" => $news));
		}
		$this->db->where("agency_id", $this->userid);
		$content['faqs'] 			= $this->db->get("faqs")->result();
		$data['sidemenu'] 			= $this->load->view("admin/agency_sidemenu", $var1, true);
		$data['content'] 			= $this->load->view("admin/agency", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function about_us($data = array())
	{
		$var1  						= array();
		$content  					= array();
		$content['pageHeader'] 		= "About Us";
		$content['id'] 				= $this->userid;
		$var1['id']					= $this->userid;
		if($this->input->post()){
			$about_us = $this->input->post("about_us");
			
			$this->db->where("agency_id", $this->userid);
			$this->db->update("agency_settings", array("about_us" => $about_us));
		}
		
		$this->db->where("agency_id", $this->userid);
		$content['about_us'] 		= $this->db->get("agency_settings")->result();
		$data['sidemenu'] 			= $this->load->view("admin/agency_sidemenu", $var1, true);
		$data['content'] 			= $this->load->view("admin/agency_about_us", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	private function do_upload($files = array()){
		$arrFile = array();
		foreach ($files as $index => $value)
        {
            if ($value['name'] != '')
            {
				$config = array();
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload');
                $this->upload->initialize($config);

                //upload the image
                if ($this->upload->do_upload($index))
                {
					$data[$index] = $this->upload->data();
					if($index == "logo"){
						$arrFile[$index] = array($data[$index]['file_name']);
					}else if($index == "banner"){
						$arrFile[$index] = array($data[$index]['file_name']);
					}
                }
            }
        }
		
		return $arrFile;
	}
	
	public function settings($data = array())
	{
		$var1  						= array();
		$content  					= array();
		$content['pageHeader'] 		= "Settings";
		$content['id'] 				= $this->userid;
		$var1['id']					= $this->userid;
		if($this->input->post()){
			$license	= $this->input->post("license"); 	
			$arrFiles 	= $this->do_upload($_FILES);
			$arrInsert = array();
			$arrInsert['serial_no'] = $license;
			if(isset($arrFiles["logo"])){
				$arrInsert['logo'] = $arrFiles["logo"][0];
			}
			if(isset($arrFiles["banner"])){
				$arrInsert['banner'] = $arrFiles["banner"][0];
			}
			$this->db->where("agency_id", $this->userid);
			$this->db->update("agency_settings", $arrInsert);
			
			$content['submit'] 	= true;
			$content['class'] 		= "alert alert-success";
			$content['dismiss'] 	= "alert";
			$content['errType'] 	= "Success";
			$content['msg'] 		= "New Settings has been updated";
		}
		
		$content['data']			= $this->db->get_where("agency_settings", array("agency_id" => $this->userid))->result();
		$data['sidemenu'] 			= $this->load->view("admin/agency_sidemenu", $var1, true);
		@$data['content'] 			= $this->load->view("admin/agency_settings", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function new_agency($data = array())
	{
		$var1  		= array();
		$content  	= array();
		$isExist 		= false;
		$intUserID 	= 0;
		if($this->input->post()){
			foreach($this->input->post() as $key=>$val):
				$arrInput[$key] 	= $val;
			endforeach;
				$isExist 				= $this->Mod_user->hasData($arrInput['user']['username']);
			if(!$isExist){
				$arrUserCredentials['username'] 	= $arrInput['user']['username'];
				$arrUserCredentials['password'] 	= md5($arrInput['user']['password']);
				$arrUserCredentials['user_type'] 	= 2; // Agency
				$intUserID = $this->Mod_user->add($arrUserCredentials);
				
				$arrInput['credentials']['userid'] 	= $intUserID;
				
				$content['submit'] 		= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Agency has been added";
				
				$this->Mod_agency->add($arrInput['credentials']);
				
				$this->Mod_agency->add(array("agency_id" => $intUserID), "faqs");
				$this->Mod_agency->add(array("agency_id" => $intUserID), "agency_settings");
			}else{
				$content['submit'] 	= true;
				$content['class'] 		= "alert alert-danger";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Error";
				$content['msg'] 		= "Agency already exist";
			}
		}
		
		$content['country']			= $this->Mod_agency->getCountries();
		$content['pageHeader'] 		= "New Agency";
		$data['sidemenu'] 			= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 			= $this->load->view("admin/agency_new", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function agency_list($data = array())
	{
		$var1  = array();
		$content  = array();
		
		$content['data'] 				= $this->Mod_agency->get_data();
		$content['status'] 			= array("active","inactive","suspended");
		$content['pageHeader'] 	= "Agency List";

		$data['sidemenu'] 			= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 				= $this->load->view("admin/agency_list", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function delete($id = 0){
		$id = $this->input->post("delete");
		$this->Mod_agency->delete($id);
		redirect("admin/agency/agency_list");
	}
	
	public function status($status = ""){
		$status 	= $this->input->get("action");
		$userid 	= $this->input->get("id");
		$this->Mod_agency->updateStatus($status, $userid);
		
		redirect("admin/agency/agency_list");
	}
	
	public function view($userid = 0){
		$var1  							= array();
		$content  						= array();
		$content['country']			= $this->Mod_agency->getCountries();
		$content['pageHeader'] 	= "View " . $this->input->get("username");
		$content['data'] 				= $this->Mod_agency->get_agency($this->input->get("id"));
		if($this->input->post()){
			foreach($this->input->post() as $key=>$val):
				$arrInput[$key] 	= $val;
			endforeach;

				$content['submit'] 	= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Agency has been Updated";
				$this->session->set_flashdata('flsh_msg', $content);
				$this->Mod_agency->update($arrInput['credentials'], $this->input->get("id"));
				redirect("admin/agency/view?id=".$this->input->get('id')."&username=".$this->input->get("username")."");
		}
		$data['sidemenu'] 			= $this->load->view("admin/sidemenu", $var1, true);
		$data['content'] 				= $this->load->view("admin/agency_view", $content, true);
		$this->load->view('admin/index', $data);
	}

	public function profiles(){
		$var1  						= array();
		$content  					= array();
		$content['pageHeader'] 		= "Profiles";
		$content['id'] 				= $this->userid;
		$var1['id']					= $this->userid;
		
		
		$content['data']			= $this->Mod_profile->getProfileByAgencyID($this->userid);
		$data['sidemenu'] 			= $this->load->view("admin/agency_sidemenu", $var1, true);
		$data['content'] 			= $this->load->view("admin/agency_profiles", $content, true);
		$this->load->view('admin/index', $data);
	}
	
	public function view_profile(){
		
		if($this->session->userdata("userid") == $this->input->get("agency_id")){
		$var1  = array();
		$content = array();
		$var1['id']							= $this->userid;
		$content['profile_id']				= $this->input->get("profile_id");
		$content['agency_id']				= $this->input->get("agency_id");
		$content['pageHeader'] = "View Profile";
		$info['rel']					= array('CHRISTIANITY', 'ISLAM', 'HINDUISM', 'TAOISM', 'BUDDHISM', 'SHINTO', 'SIKHISM', 'JUDAISM', 'AGNOSTIC', 'ATHEIST');
		$info['educ']					= array('NONE', 'COMPLETED PRIMARY SCHOOL', 'COMPLETED', 'SECONDARY SCHOOL', 'DIPLOMA', 'DEGREE', 'PHD', 'CERTIFICATE OF COMPLETION');
		$info['stat']					= array('SINGLE', 'IN A RELATIONSHIP', 'ENGAGED', 'MARRIED', 'DIVORCED', 'WIDOWED');
		$info['illness']				= array('Mental illness', 'Tuberculosis', 'Epilepsy', 'Heart disease', 'Asthma', 'Malaria', 'Diabetes', 'Operations', 'Hypertension', 'Others');
		$info['food']					= array('No pork', 'No beef', 'Vegetarian', 'Others');
		$info['eval']					= array('Based on FDWs declaration, no evaluation/observation by Singapore EA or overseas trainingcentre/EA', 
													'Interviewed by Singapore EA', 'Interviewed by overseas training centre/EA');
		
		$info['info']				= $this->Mod_profile->view_profile($this->session->userdata("userid"), $this->input->get("profile_id"));
		$info['country']			= $this->Mod_agency->getCountries();
		$info['agencies']			= $this->Mod_profile->get_agencies();

		
		$content['personal_info']			= $this->load->view("agency/profile/personal_info",$info,true);
		$content['medical_history']			= $this->load->view("agency/profile/medical_history",$info,true);
		$content['area_of_work']			= $this->load->view("agency/profile/area_of_work",$info,true);
		$content['employment_history']		= $this->load->view("agency/profile/employment_history",$info,true);
		$content['media']					= $this->load->view("agency/profile/media",$info,true);
	

		if($this->input->post()){
			$post = $this->input->post();
		
			$intProfileID = $this->input->get("profile_id");
			$intAgencyID = $this->input->get("agency_id");
			
			$arrWhere = array("id" => $intProfileID);
			$arrWhere1 = array("profile_id" => $intProfileID);
			
			$this->Mod_profile->update($post['personal_info'], $arrWhere, "profile_personal_info");
			$personal_medical['personal_medical'] = $post['personal_medical'];
			
			$personal_medical['personal_medical']['illness'] 				= isset($post['personal_medical']['illness']) ? implode("|", $post['personal_medical']['illness']) : "";
			$personal_medical['personal_medical']['evaluation_skill'] 		= isset($post['personal_medical']['evaluation_skill']) ?  implode("|", $post['personal_medical']['evaluation_skill']) : "";
			
			$this->Mod_profile->update($personal_medical['personal_medical'], $arrWhere1, "profile_medical_history");

			$post['personal_area_of_work']['profile_id'] = $intProfileID;
			$this->Mod_profile->update($post['personal_area_of_work'], $arrWhere1, "profile_area_of_work");
			
			#print_r($post['employment_history']['history']);
			// for($x = 0;$x <= count($post['employment_history']['history']); $x++){
				// if($post['employment_history']['history']['employer'][$x] <> "" AND $post['employment_history']['history']['date_from'][$x] <> "" AND $post['employment_history']['history']['date_to'][$x]
					// AND $post['employment_history']['history']['work_duties'][$x] <> "" AND $post['employment_history']['history']['remarks'][$x] <> "" AND $post['employment_history']['history']['emp_feedback'][$x] <> ""){
					// $employment_history['employment_history']['employer'] 			= $post['employment_history']['history']['employer'][$x];
					// $employment_history['employment_history']['date_from'] 		= $post['employment_history']['history']['date_from'][$x];
					// $employment_history['employment_history']['date_to'] 			= $post['employment_history']['history']['date_to'][$x];
					// $employment_history['employment_history']['work_duties'] 		= $post['employment_history']['history']['work_duties'][$x];
					// $employment_history['employment_history']['remarks'] 			= $post['employment_history']['history']['remarks'][$x];
					// $employment_history['employment_history']['emp_feedback'] 	= $post['employment_history']['history']['emp_feedback'][$x];
					// $employment_history['employment_history']['profile_id'] 			= $intProfileID;
					// $employment_history['employment_history']['country_history'] 	= $post['employment_history']['history']['country_history'][$x];
					// $employment_history['employment_history']['prev_work_sg']	= $post['employment_history']['prev_work_sg'];
					// $employment_history['employment_history']['availability_fdw']	= isset($post['employment_history']['availability_fdw']) ? json_encode($post['employment_history']['availability_fdw']) : false ;
					// $arrWhere2 = array("id" => $post['employment_history']['history']['emp_id'][$x], "profile_id" => $intProfileID);
					// $this->Mod_profile->update($employment_history['employment_history'], $arrWhere2, "profile_employment_history");
				// }		
			// }
				$content['submit'] 	= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Profile has been Updated";
				$this->session->set_flashdata('flsh_msg', $content);

			redirect("admin/agency/view_profile?agency_id={$intAgencyID}&profile_id={$intProfileID}");
		}
		
		$data['sidemenu'] 	= $this->load->view("admin/agency_sidemenu", $var1, true);
		$data['content'] 	= $this->load->view("admin/profile_view", $content, true);

		$this->load->view('admin/index', $data);	
		}else{
			show_404(); 
		}
	}
	
	public function publish(){
		if($this->session->userdata("userid") == $this->input->get("agency_id")){
			$profile_id = $this->input->get("profile_id");
			$agency_id = $this->input->get("agency_id");
			$value 		= $this->input->get("value");
			
			$this->db->where("id", $profile_id);
			$this->db->where("profile_owner", $agency_id);
			$this->db->update("profile_personal_info", array("isPublish" => $value));
			
				$content['submit'] 	= true;
				$content['class'] 		= "alert alert-success";
				$content['dismiss'] 	= "alert";
				$content['errType'] 	= "Success";
				$content['msg'] 		= "Profile has been ".($value == 1 ? "Published" : "Unpublished")."";
				$this->session->set_flashdata('flsh_msg', $content);
			redirect("/admin/agency/profiles?id=".$agency_id."");
		}else{
			show_404();
		}
	}
	
	public function news($data = array())
	{
		$var1  						= array();
		$content  					= array();
		$content['pageHeader'] 		= "News & Announcement";
		$content['id'] 				= $this->userid;
		$var1['id']					= $this->userid;
		if($this->input->post()){
			$news = $this->input->post("news");
			
			$this->db->where("agency_id", $this->userid);
			$this->db->update("agency_settings", array("news" => $news));
		}
		
		$this->db->where("agency_id", $this->userid);
		$content['news'] 		= $this->db->get("agency_settings")->result();
		$data['sidemenu'] 			= $this->load->view("admin/agency_sidemenu", $var1, true);
		$data['content'] 			= $this->load->view("admin/agency_news", $content, true);
		$this->load->view('admin/index', $data);
	}
	
}

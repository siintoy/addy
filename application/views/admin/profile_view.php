<link href="<?php echo base_url(); ?>css/jquery-ui.css" rel="stylesheet">

<div style="padding:5px;">
    <div class="row">
	<?php if(isset($profile)): ?>
		<form role="form" method="post" action="<?php echo base_url();?>index.php/admin/profile/profile_list?owner_id=<?php echo $owner_id;?>&profile_id=<?php echo $profile_id; ?>">
	<?php else: ?>
		<form role="form" method="post" action="<?php echo base_url();?>index.php/admin/agency/view_profile?agency_id=<?php echo $agency_id;?>&profile_id=<?php echo $profile_id; ?>">
	<?php endif; ?>		
			<div class="col-md">
				<div class="panel with-nav-tabs panel-default">
					<div class="panel-heading">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1default" data-toggle="tab">Personal Information</a></li>
							<li><a href="#tab2default" data-toggle="tab">Medical History/Dietary Restrictions</a></li>
							<li><a href="#tab3default" data-toggle="tab">Area of Work</a></li>
							<li><a href="#tab5default" data-toggle="tab">Profile Picture & Video</a></li> 
							<li><a href="#tab4default" data-toggle="tab">Employment History</a></li>
						</ul>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane fade in active" id="tab1default"><?php echo isset($personal_info) ? $personal_info : false; ?></div>
							<div class="tab-pane fade" id="tab2default"><?php echo isset($medical_history) ? $medical_history : false; ?></div>
							<div class="tab-pane fade" id="tab3default"><?php echo isset($area_of_work) ? $area_of_work : false; ?></div>
							<div class="tab-pane fade" id="tab5default"><?php echo isset($media) ? $media : false; ?></div>
							</form>
							<div class="tab-pane fade" id="tab4default"><?php echo isset($employment_history) ? $employment_history : false; ?></div>
						</div>
					</div>
				</div>
			</div>
	
	</div>
</div>

<script src="<?php echo base_url(); ?>external/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script>
$( ".datepicker" ).datepicker({
	'dateFormat': 'yy-mm-dd'
});

for(x = 0;x <= 10;x++){
	$( ".datepicker"+x ).datepicker();
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

</script>

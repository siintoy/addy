/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.13-MariaDB : Database - profilexchange
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`profilexchange` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `profilexchange`;

/*Table structure for table `recruiter` */

DROP TABLE IF EXISTS `recruiter`;

CREATE TABLE `recruiter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `address` text,
  `phone` varchar(11) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `contact_person` varchar(32) DEFAULT NULL,
  `contact_person_no` varchar(11) DEFAULT NULL,
  `contact_person_email` varchar(32) DEFAULT NULL,
  `interview` varchar(12) DEFAULT NULL,
  `bus_reg_no` varchar(11) DEFAULT NULL,
  `url` text,
  `bank_account_no` varchar(30) DEFAULT NULL,
  `bank_account_name` varbinary(32) DEFAULT NULL,
  `no_booking_allowed` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`,`username`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `recruiter` */

insert  into `recruiter`(`id`,`username`,`password`,`name`,`address`,`phone`,`country`,`contact_person`,`contact_person_no`,`contact_person_email`,`interview`,`bus_reg_no`,`url`,`bank_account_no`,`bank_account_name`,`no_booking_allowed`,`status`) values (1,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(2,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(3,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(4,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(5,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active'),(6,'Username','Password','Recruiter Name','Recruiter Address','Recruiter P','one','Contact Person','Contact Per','dasdas@sadsa.com',' Interview P','Recruiter B','Recruiter URL','Bank Account No. ','Bank Account Name ',0,'active');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

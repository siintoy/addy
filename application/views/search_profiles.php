<div class="container-fluid">
	<div style="padding:5px;"class="col-xs-12">
	<h3>Available Profiles</h3>
	<div style="padding:10px;"class="col-xs-13">
		<?php if(count($profile) > 0): ?>
			<?php foreach($profile as $l): ?>
				<div class="lastest_profile" style="margin:5px;border:1px solid #ccc; height:200px;width:15%;">
					<a target="_blank" href="<?php echo site_url("main/view_profile?profile_id=".$l->profile_id."&id=".$id."");?>">
						<img style="height:100px;width:100%" src="<?php echo base_url(); ?>uploads/<?php echo $l->picture; ?>"/></a>
					<div style="padding:5px;"> 
						<?php echo $l->profname; ?> <br> 
						<?php echo (isset($l->country_name) ? $l->country_name ."<br>": false); ?>  
						Age: <?php echo $l->age; ?> <br> 
						<?php echo ucwords(strtolower($l->religion)); ?>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<i>No Profile(s) Available</i>
		<?php endif; ?>
	</div>
</div>
<br><br>
</div>